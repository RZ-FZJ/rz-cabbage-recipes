<Cabbage>
form caption("Etude #3") size(551, 380), guiMode("queue"), pluginId("etu3"), colour(64, 96, 160), fontColour(240, 240, 240)

rslider bounds(12, 14, 70, 70), channel("freq"), range(1, 1e4, 440, 0.3), text("Frequency")
rslider bounds(106, 14, 30, 30), channel("frvar"), range(1e-2, 1e2, 1, 0.3)
rslider bounds(106, 49, 30, 30), channel("frvart"), range(0, 1, 0.5)
; Use this to check aliasing (sr=44100):
;rslider bounds(12, 14, 70, 70), channel("freq"), range(4310, 4510, 4410), text("Frequency")
; Use this to check aliasing (sr=48000):
;rslider bounds(12, 14, 70, 70), channel("freq"), range(4700, 4900, 4800), text("Frequency")
button bounds(82, 14, 20, 20), channel("ffollow"), colour:1("red"), text("F")
rslider bounds(144, 14, 70, 70), channel("width"), range(0, 10, 0.05, 0.3), text("Width")
rslider bounds(218, 14, 30, 30), channel("wivar"), range(1e-2, 1e2, 1, 0.3)
rslider bounds(218, 49, 30, 30), channel("wivart"), range(0, 1, 0.5)
vslider bounds(258, 9, 40, 70), channel("flim"), range(0, 10, 0), text("Lim")
button bounds(308, 14, 20, 20), channel("gorw"), text(""), colour:1("red")
label bounds(331, 17, 120, 14), align("left"), text("Gamma/Weybull")
button bounds(308, 37, 20, 20), channel("bipolar"), colour:1("red"), text("")
label bounds(331, 40, 120, 14), align("left"), text("Bipolar")
button bounds(308, 60, 20, 20), channel("antia"), colour:1("red"), text(""), value(1)
label bounds(331, 63, 120, 14), align("left"), text("Anti-Aliasing")
rslider bounds(454, 14, 70, 70), channel("amprand"), range(0, 1, 0), text("Amp. Rand.")

rslider bounds(12, 94, 70, 70), channel("att"), range(0.01, 1, 0.02, 0.5), text("Attack")
rslider bounds(82, 94, 70, 70), channel("dec"), range(0, 1, 0.5, 1), text("Decay")
rslider bounds(152, 94, 70, 70), channel("sus"), range(0, 1, 1, 0.5), text("Sustain")
rslider bounds(222, 94, 70, 70), channel("rel"), range(0, 5, 0.02, 1), text("Release")
combobox bounds(422, 94, 100, 25), channelType("string"), channel("snaps"), populate("*.snaps")
filebutton bounds(422, 130, 25, 25), channel("save"), text("+"), mode("named snapshot"), populate("*.snaps")
filebutton bounds(452, 130, 25, 25), channel("remove"), text("-"), mode("remove preset"), populate("*.snaps")

rslider bounds(12, 174, 70, 70), channel("lpfreq"), range(1, 1e5, 1e5, 0.3), text("Low Pass")
button bounds(82, 174, 20, 20), channel("lpfollow"), colour:1("red"), text("F")
rslider bounds(106, 174, 30, 30), channel("lpvar"), range(1e-2, 1e2, 1, 0.3)
rslider bounds(106, 209, 30, 30), channel("lpvart"), range(0, 1, 0.5)
rslider bounds(144, 174, 70, 70), channel("reson"), range(1, 500, 1, 0.5), text("Resonance")
rslider bounds(222, 174, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(292, 174, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(362, 174, 70, 70), channel("wet"), range(0, 1, 0, 1, .01), text("Dry/Wet")
rslider bounds(432, 174, 70, 70), channel("volume"), range(0.1, 100, 1, 0.5), text("Volume")
vslider bounds(500, 169, 40, 75), channel("vumeter"), range(0, 2, 1, 0.5), text("VU"), colour(0,0,0,0)

button bounds(8, 268, 20, 95), channel("play"), colour:1("red"), text("P")
keyboard bounds(33, 268, 500, 95), middleC(4), value(35)
</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
; Initialize the global variables.
ksmps = 1
nchnls = 2
0dbfs = 1


opcode gamrand, k, kk

kalpha, klambda xin

if kalpha>1 then
  kd = kalpha - 0.333333333333
  kc = 0.333333333333/sqrt(kd)
  reject1:
    kz gauss 0, 1
    kv = 1+kc*kz
    if kv<=0 kgoto reject1
    kv = kv*kv*kv
    ku rnd 1
    if log(ku)>(0.5*kz*kz+kd-kd*kv+kd*log(kv)) kgoto reject1
  kx = kd*kv/klambda
else
  kd = kalpha + 0.666666666667
  kc = 0.333333333333/sqrt(kd)
  reject2:
    kz gauss 0, 1
    kv = 1+kc*kz
    if kv<=0 kgoto reject2
    kv = kv*kv*kv
    ku rnd 1
    if log(ku)>(0.5*kz*kz+kd-kd*kv+kd*log(kv)) kgoto reject2
  ku1 rnd 1
  kx = kd*kv*(ku1^(1/kalpha))/klambda
endif

xout kx

endop


gaFilt init 0


instr 1

iwindex ftgen 0, 0, 64, 7, 0, 64
kpposf init 0
kpposl init 0
aDry init 0

kwidth chnget "width"
kfreq chnget "freq"
iffollow chnget "ffollow"
ifrvar chnget "frvar"
ifrvart chnget "frvart"
ilpfollow chnget "lpfollow"
kVolume chnget "volume"
if iffollow>0 then
  kfreq = 2.2727273e-3*kfreq*p4
endif
iamp = p5
kweyb chnget "gorw"
iwivar chnget "wivar"
iwivart chnget "wivart"
kWienv expsegr iwivar, iwivart, 1, 0 ,1
kwidth = kWienv*kwidth
kFrenv expsegr ifrvar, ifrvart, 1, 0 ,1
kfreq = kFrenv*kfreq
koff = 0.999834*kfreq/sr ; offset matched to weight of pulse
if kwidth>0 then
  if kweyb>0 then
    kwindex = 0.1*kwidth
    kgamma table3 kwindex, 1001, 1
    kfac table3 kwindex, 1002, 1
    kscale = kfac/kfreq
  else
    kalpha = 1/(kwidth*kwidth)
    klambda = kalpha*kfreq
  endif
endif
karand chnget "amprand"
kbip chnget "bipolar"
iantia chnget "antia"
kflim chnget "flim"
iAtt chnget "att"
iDec chnget "dec"
iSus chnget "sus"
iRel chnget "rel"
klpfreq chnget "lpfreq"
ilpvar chnget "lpvar"
ilpvart chnget "lpvart"
kreson chnget "reson"
if ilpfollow>0 then
  klpfreq = 2.2727273e-3*klpfreq*p4
endif

kcounter init 0
kpol init 1

kEnv transegr 0, iAtt, 0, 1, 10-iAtt, (iAtt-10)/iDec, iSus, iRel, -10,0

test:
if kcounter>0 kgoto silent

tablew 10.6666666667*(1-kcounter), kpposl, iwindex, 0, 0, 1
kpposl = kpposl+1
;printks "%i %i\n", 0, kpposf, kpposl

if kwidth>0 then
  if kweyb>0 then
    kx exprand 1
    ky = kx^kgamma*kscale
  else
    ky gamrand kalpha,klambda
  endif
else
  ky = 1/kfreq
endif

if kbip==0 then
  kcounter = kcounter + sr*ky
  kpol = 1
else
  kcounter = kcounter + 0.5*sr*ky
  kpol = -kpol
endif

if karand>0 then
  karnd random 0,1
  karnd = karnd^karand
else
  karnd = 1
endif

kgoto test

silent:
if kflim>0 then
  kcounter = min(kcounter-1, sr/kflim)
else
  kcounter = kcounter-1
endif

kppos = kpposl-1
kpol1 = kpol
if kbip==0 then
  aDry = -koff
else
  aDry = 0
endif
if (kpposl!=kpposf) then
  ploop:
    kindex table kppos, iwindex, 0, 0, 1
    asig tablei kindex, 1011-iantia
    aDry = aDry + kpol1*karnd*asig
    kindex = kindex+10.66666667
    tablew kindex, kppos, iwindex, 0, 0, 1
    if (kindex>127) then
      kpposf = kpposf+1
    endif
    kppos = kppos-1
    if kbip!=0 then
      kpol1 = -kpol1
    endif
  if kppos>=kpposf goto ploop
endif
aDry = iamp*kVolume*kEnv*(440/kfreq)*aDry

; low pass
if (klpfreq<1e5) then
  kLpenv expsegr ilpvar, ilpvart, 1, 1, 1
  aFilt lowpass2 aDry, kLpenv*klpfreq, kreson
  gaFilt = gaFilt+aFilt
else
  gaFilt = gaFilt+aDry
endif

endin


; This instrument adds reverb and background tasks:

instr 110

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"

aLrev, aRrev freeverb gaFilt, gaFilt, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaFilt + kWet*aLrev
aRtot = kDry*gaFilt + kWet*aRrev

outs aLtot, aRtot

; play continuously if P is pressed
kplay chnget "play"
if changed(kplay)>0 then
  if kplay>0 then
;   printk -1, kplay
    igoto skip
    event "i", 1, 0, -1, 440, 1
    cabbageSetValue "ffollow", 0, kplay
    cabbageSetValue "lpfollow", 0, kplay
    skip:
  else
    turnoff2 1, 8, 0
  endif
endif

; set frequency/LP frequency to tuned value when F is pressed
kffollow chnget "ffollow"
klpfollow chnget "lpfollow"
ksnaps cabbageGet "snaps"
if changed(kffollow)>0 then
  cabbageSetValue "freq", 440, kffollow
endif
if changed(klpfollow)>0 then
  cabbageSetValue "lpfreq", 440, klpfollow
endif

; the VU meter
ktrig metro 25
kVU max_k max(aLtot,aRtot), ktrig, 1
if (ktrig==1) then
  cabbageSetValue "vumeter", kVU
  if (kVU>1) then
    cabbageSet 1, "vumeter", "trackerColour(255,0,0)"
  else
    cabbageSet 1, "vumeter", "trackerColour(0,255,0)"
  endif
endif

clear gaFilt

endin

</CsInstruments>
<CsScore>
;i1 0 z
i110 0 z
f 1001 0 129 -2 0.00000 0.06357 0.13213 0.20510 0.28175 0.36127 0.44286 0.52570 0.60908 \
0.69238 0.77505 0.85669 0.93695 1.01559 1.09244 1.16737 1.24034 1.31129 1.38023 1.44718 1.51218 \
1.57528 1.63653 1.69598 1.75372 1.80979 1.86427 1.91722 1.96870 2.01879 2.06752 2.11497 2.16119 \
2.20623 2.25014 2.29297 2.33476 2.37555 2.41540 2.45433 2.49238 2.52959 2.56599 2.60162 2.63649 \
2.67065 2.70411 2.73691 2.76906 2.80059 2.83153 2.86189 2.89169 2.92095 2.94969 2.97793 3.00568 \
3.03296 3.05979 3.08617 3.11212 3.13766 3.16280 3.18755 3.21192 3.23591 3.25956 3.28285 3.30581 \
3.32843 3.35074 3.37274 3.39444 3.41584 3.43695 3.45778 3.47834 3.49864 3.51867 3.53845 3.55799 \
3.57729 3.59635 3.61518 3.63378 3.65217 3.67034 3.68831 3.70607 3.72362 3.74099 3.75816 3.77514 \
3.79194 3.80856 3.82501 3.84128 3.85738 3.87332 3.88910 3.90472 3.92018 3.93549 3.95064 3.96565 \
3.98052 3.99525 4.00983 4.02428 4.03860 4.05279 4.06684 4.08077 4.09457 4.10826 4.12182 4.13526 \
4.14859 4.16180 4.17490 4.18789 4.20077 4.21355 4.22621 4.23878 4.25124 4.26361 4.27587 4.28804
f 1002 0 129 -2 1.00000 1.03403 1.06477 1.09071 1.11060 1.12351 1.12898 1.12698 1.11785 \
1.10227 1.08108 1.05523 1.02568 0.99335 0.95908 0.92358 0.88748 0.85129 0.81542 0.78018 0.74581 \
0.71249 0.68035 0.64945 0.61986 0.59158 0.56462 0.53895 0.51455 0.49137 0.46938 0.44851 0.42873 \
0.40998 0.39222 0.37538 0.35941 0.34429 0.32994 0.31634 0.30344 0.29120 0.27957 0.26854 0.25806 \
0.24809 0.23862 0.22961 0.22104 0.21288 0.20511 0.19770 0.19064 0.18390 0.17748 0.17134 0.16548 \
0.15988 0.15453 0.14941 0.14452 0.13983 0.13534 0.13104 0.12692 0.12297 0.11918 0.11554 0.11205 \
0.10870 0.10548 0.10238 0.09941 0.09655 0.09379 0.09114 0.08859 0.08613 0.08377 0.08149 0.07929 \
0.07716 0.07512 0.07314 0.07124 0.06940 0.06762 0.06590 0.06424 0.06264 0.06108 0.05958 0.05813 \
0.05672 0.05536 0.05405 0.05277 0.05153 0.05033 0.04917 0.04805 0.04696 0.04590 0.04487 0.04387 \
0.04291 0.04197 0.04105 0.04017 0.03931 0.03847 0.03766 0.03687 0.03610 0.03535 0.03463 0.03392 \
0.03323 0.03256 0.03191 0.03128 0.03066 0.03006 0.02947 0.02890 0.02834 0.02780 0.02727 0.02676
f 1010 0 128 -2 -0.00000 -0.00024 -0.00060 -0.00105 -0.00156 -0.00206 -0.00245 -0.00262 \
-0.00249 -0.00194 -0.00093 0.00054 0.00243 0.00460 0.00685 0.00892 0.01051 0.01129 0.01097 0.00934 \
0.00626 0.00178 -0.00392 -0.01044 -0.01724 -0.02362 -0.02880 -0.03199 -0.03245 -0.02961 -0.02318 \
-0.01318 0.00000 0.01554 0.03225 0.04861 0.06289 0.07330 0.07812 0.07594 0.06581 0.04743 0.02122 \
-0.01153 -0.04872 -0.08749 -0.12437 -0.15546 -0.17679 -0.18455 -0.17543 -0.14696 -0.09777 -0.02777 \
0.06172 0.16795 0.28686 0.41325 0.54111 0.66400 0.77543 0.86935 0.94052 0.98491 1.00000 0.98491 \
0.94052 0.86935 0.77543 0.66400 0.54111 0.41325 0.28686 0.16795 0.06172 -0.02777 -0.09777 -0.14696 \
-0.17543 -0.18455 -0.17679 -0.15546 -0.12437 -0.08749 -0.04872 -0.01153 0.02122 0.04743 0.06581 \
0.07594 0.07812 0.07330 0.06289 0.04861 0.03225 0.01554 0.00000 -0.01318 -0.02318 -0.02961 \
-0.03245 -0.03199 -0.02880 -0.02362 -0.01724 -0.01044 -0.00392 0.00178 0.00626 0.00934 0.01097 \
0.01129 0.01051 0.00892 0.00685 0.00460 0.00243 0.00054 -0.00093 -0.00194 -0.00249 -0.00262 \
-0.00245 -0.00206 -0.00156 -0.00105 -0.00060 -0.00024
; Use this to deliberately create aliasing:
f 1011 0 128 7 0 58 0 0 1 11 1
</CsScore>
</CsoundSynthesizer>
