; Bugs:
; - follows from snaps overwrite frequencies (same in Etude #3)
; Features to implement:
; - ks=32 subinstrument/UDO

<Cabbage>
form caption("Fractal 1") size(551, 380), guiMode("queue"), pluginId("frc1"), colour(64, 96, 160), fontColour(240, 240, 240)

label bounds(349, 368, 200, 10), align('right'), text("Version 1.0 2024-11-14")

button bounds(82, 14, 22, 22), channel("ffollow"), colour:1("red"), text("F")
;rslider bounds(12, 14, 70, 70), channel("freq"), range(1, 1e4, 440, 0.5), text("Frequency")
rslider bounds(12, 14, 70, 70), channel("note"), range(-12, 144, 69, 1, 1), text("Frequency")
button bounds(82, 47, 22, 22), channel("fcorr"), colour:1("red"), text("C")
rslider bounds(109, 14, 70, 60), channel("fract"), range(0, 0.999, 0)
label bounds(99, 71, 90, 14), channel("df"), fontStyle("plain"), text("df = 1.0000")
button bounds(179, 14, 22, 22), channel("mdf"), text("M"), colour:1("red")
rslider bounds(204, 14, 70, 70), channel("depth"), range(0, 12, 0, 1, 1), text("Depth")
button bounds(289, 14, 20, 20), channel("oneshot"), text(""), colour:1("red")
label bounds(312, 17, 120, 14), align("left"), fontStyle("plain"), text("One Shot")
button bounds(289, 47, 20, 20), channel("bipolar"), colour:1("red"), text("")
label bounds(312, 50, 120, 14), align("left"), fontStyle("plain"), text("Bipolar")

rslider bounds(12, 94, 70, 70), channel("att"), range(0.001, 1, 0.02, 0.5), text("Attack")
rslider bounds(87, 94, 70, 70), channel("dec"), range(0, 1, 0.5, 1), text("Decay")
rslider bounds(162, 94, 70, 70), channel("sus"), range(0, 1, 1, 0.5), text("Sustain")
rslider bounds(237, 94, 70, 70), channel("rel"), range(0, 5, 0.02, 1), text("Release")

button bounds(82, 174, 22, 22), channel("lpfollow"), colour:1("red"), text("F")
rslider bounds(12, 174, 70, 70), channel("lpfreq"), range(10, 2e4, 2e4, 0.2), text("Low Pass")
rslider bounds(106, 174, 30, 30), channel("lpvar"), range(1e-2, 1e2, 1, 0.3)
rslider bounds(106, 209, 30, 30), channel("lpvart"), range(0, 1, 0.5)
rslider bounds(144, 174, 70, 70), channel("reson"), range(1, 500, 1, 0.5), text("Resonance")
rslider bounds(222, 174, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(292, 174, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(362, 174, 70, 70), channel("wet"), range(0, 1, 0, 1, .01), text("Dry/Wet")
rslider bounds(432, 174, 70, 70), channel("volume"), range(-40, 40, -6), text("Volume")
vslider bounds(500, 169, 40, 75), channel("vumeter"), range(0, 2, 1, 0.2), text("VU"), colour(0,0,0,0)

combobox bounds(403, 14, 120, 22), channelType("string"), channel("snaps"), populate("fractal1.snaps")
filebutton bounds(471, 47, 22, 22), channel("save"), text("+"), mode("named snapshot"), populate("fractal1.snaps")
filebutton bounds(501, 47, 22, 22), channel("remove"), text("-"), mode("remove preset"), populate("fractal1.snaps")

button bounds(8, 268, 20, 95), channel("play"), colour:1("red"), text("P")
keyboard bounds(33, 263, 500, 95), middleC(4), value(35)

</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
; Initialize the global variables.
ksmps = 1
nchnls = 2
0dbfs = 1


gaFilt init 0
; Table of fractal pulse times (example, will be overwritten):
gidtab ftgen 0, 0, 32, -2, 1, 2, 5, 14, 41, 122, 365, 1094, 3281, 9842, 29525, 88574, 265721, 797162, 2391485, 7174454, 21523361, 64570082, 193710245, 581130734, 1743392201, 5230176602
gkdf init 1


instr 1

; This table contains the positions in the antialiasing wavetable for all playing notes:
iwindex ftgen 0, 0, 64, 7, 0, 64
; Indices (in table #inwindex) of the first and last playing note:
kpposf init 0
kpposl init 0

knote chnget "note"
kfreq mtof knote
iffollow chnget "ffollow"
ifcorr chnget "fcorr"
idepth1 chnget "depth"
idepth = round(idepth1)
ioneshot chnget "oneshot"
ilpfollow chnget "lpfollow"
kdBvol chnget "volume"
kVolume ampdb kdBvol
if iffollow>0 then
  kfreq = 2.2727273e-3*kfreq*p4
  iamp = p5
else
  iamp = 1
endif
kbip chnget "bipolar"
iAtt chnget "att"
iDec chnget "dec"
iSus chnget "sus"
iRel chnget "rel"
klpfreq chnget "lpfreq"
ilpvar chnget "lpvar"
ilpvart chnget "lpvart"
kreson chnget "reson"
if ilpfollow>0 then
  klpfreq = 2.2727273e-3*klpfreq*p4
endif

kcounter init 0
kpol init 1
if ifcorr=0 then
  kd = 1/kfreq
else
  kd = 2^(idepth*(1-1/gkdf))/kfreq
endif
itl = 2^idepth
km init 0

; i-time version of df:
;ia = 2^(1/i(gkdf))
;idel = 1
;ie = 1
;iap = 1
;iindex = 0
;iloop:
;tabw_i idel, iindex, gidtab
;print idel
;iap = ia*iap
;idel = iap-ie
;ie = iap+ie
;loop_lt iindex, 1, 32, iloop

if changed(gkdf)==1 then
  ka = 2^(1/gkdf)
  kdel = 1
  ke = 1
  kap = 1
  kindex = 0
  iloop:
  tabw kdel, kindex, gidtab
  kap = ka*kap
  kdel = kap-ke
  ke = kap+ke
  loop_lt kindex, 1, 32, iloop
endif

kEnv transegr 0, iAtt, 0, 1, 10-iAtt, (iAtt-10)/iDec, iSus, iRel, -10,0

test:
if kcounter>0 kgoto silent

tablew 10.6666666667*(1-kcounter), kpposl, iwindex, 0, 0, 1
kpposl = kpposl+1

; In the following block the time distance to the next pulse is calculated:
klevel = 0
km1 = km
lloop:
if (km1&1)==0 kgoto lfound
km1 = km1>>1
klevel=klevel+1
kgoto lloop
lfound:
kdelta tab klevel, gidtab
ky = kd*kdelta
printks "%f %f %f\n", 0, km, klevel, kdelta
km = km+1
if km>=itl then
  if ioneshot==1 then
    turnoff
  else
    km=0
  endif
endif
;ky=1/1864.7

; This block advances the time counter by the time calculated before
; If bipolar: half the time, and reverse the polarity (of the most recent pulse)
if kbip==0 then
  kcounter = kcounter + sr*ky
  kpol = 1
else
  kcounter = kcounter + 0.5*sr*ky
  kpol = -kpol
endif

kgoto test

silent:
kcounter = kcounter-1
; fprintks "log.txt", "c: %i, f: %i, l: %i\n", kcounter, kpposf, kpposl

; The following block sums up the contributions from the antialiasing wavetable of all playing pulses:
; (The loop is counting down because the polarity in kpol is that of the last pulse.)
kppos = kpposl-1
kpol1 = kpol
if kbip>0 then
  araw = 0
else
  araw = -( (2/ka)^idepth / (sr*kd) )
endif
if (kpposl!=kpposf) then
  ploop:
    kindex table kppos, iwindex, 0, 0, 1
    asig table3 kindex, 1010
;   fprintks "log.txt", "i: %.2f, s: %.4f\n", kindex, kpol1*asig
    araw = araw + kpol1*asig 
    kindex = kindex+10.66666667
    tablew kindex, kppos, iwindex, 0, 0, 1
    if (kindex>127) then
      kpposf = kpposf+1
    endif
    kppos = kppos-1
    if kbip!=0 then
      kpol1 = -kpol1
    endif
  if kppos>=kpposf goto ploop
endif
; old version counting up and using one polarity:
; kppos = kpposf
; araw = 0
; if (kpposl!=kpposf) then
;   araw = 0
;   ploop:
;     kindex table kppos, iwindex ; read position in antia wavetable
;     asig table3 kindex, 1010 ; read amplitude from antia wavetable
;     araw = araw + kpol*asig ; add to total amplitude
;     kindex = kindex+10.66666667 ; increment antia wavetable index
;     tablew kindex, kppos, iwindex
;     if (kindex>127) then ; if end of antia wavetable (pulse finished)
;       kpposf = kpposf+1 ; drop first pulse in list
;       if (kpposf>=64) then ; cycle pulse list if end is reached
;         kpposf = 0
;       endif
;     endif
;     kppos = kppos+1 ; go to next pulse
;     if (kppos>=64) then ; cycle pulse list if end is reached
;       kppos = 0
;     endif
;   if kppos!=kpposl goto ploop
; endif


; fout "/Users/zorn/P/Texte/OwnMusic/Cabbage/pre_all.wav", 4 ,araw
; fout "C:\\Users\\Reiner\\Documents\\OwnMusic\\Cabbage\\pre_all.wav", 4 ,araw

araw = iamp*kVolume*kEnv*(440/kfreq)*araw

; low pass
if (klpfreq<1.9e4) then
;  kLpenv expsegr ilpvar, ilpvart, 1, 0 ,1
  kLpenv expseg ilpvar, ilpvart, 1, 1, 1
  afilt lowpass2 araw, kLpenv*klpfreq, kreson
  gaFilt = gaFilt + afilt
else
  gaFilt = gaFilt + araw
endif

endin


; This instrument adds reverb and background tasks:

instr 110

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"
kMdf chnget "mdf"

aLrev, aRrev freeverb gaFilt, gaFilt, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaFilt + kWet*aLrev
aRtot = kDry*gaFilt + kWet*aRrev

outs aLtot, aRtot
; fout "/Users/zorn/P/Texte/OwnMusic/Cabbage/output.wav", 4 ,aLtot, aRtot
fout "C:\\Users\\Reiner\\Documents\\OwnMusic\\Cabbage\\output.wav", 4 ,aLtot, aRtot

; play continuously if P is pressed
kplay chnget "play"
if changed(kplay)>0 then
  if kplay>0 then
;   printk -1, kplay
    igoto skipfsset
    event "i", 1, 0, -1
    cabbageSetValue "ffollow", 0, 1
    cabbageSetValue "lpfollow", 0, 1
    skipfsset:
  else
    turnoff2 1, 8, 0
  endif
endif

; mod wheel control of df:
; printk 0.2, kMdf
if (kMdf!=0) then
  kdfm ctrl7 1, 1, 0, 0.999
;  printk 0.2, kdfm
  cabbageSetValue "fract", kdfm, 1
endif

; convert 'fract' setting into fractal dimension:
kfract chnget "fract"
if changed(kfract)==1 then
  gkdf = 1-kfract^3
  Sdftxt sprintfk "df = %6.4f", gkdf
  cabbageSet 1, "df", "text", Sdftxt
endif

; set frequency/LP frequency to tuned value when F is pressed
; This does not work together with presets which change kffollow or klpfollow.
; They will overwrite the values from the preset
;kffollow chnget "ffollow"
;klpfollow chnget "lpfollow"
;if changed(kffollow)>0 then
;  cabbageSetValue "note", 69, kffollow
;endif
;if changed(klpfollow)>0 then
;  cabbageSetValue "lpfreq", 440, klpfollow
;endif

; the VU meter
ktrig metro 25
kVU max_k gaFilt, ktrig, 1
if (ktrig==1) then
  cabbageSetValue "vumeter", kVU
  if (kVU>1) then
    cabbageSet 1, "vumeter", "trackerColour(255,0,0)"
  else
    cabbageSet 1, "vumeter", "trackerColour(0,255,0)"
  endif
endif

clear gaFilt

endin

</CsInstruments>
<CsScore>
;i1 0 z
i110 0 z
f 1010 0 128 -2 -0.00000 -0.00024 -0.00060 -0.00105 -0.00156 -0.00206 -0.00245 -0.00262 \
-0.00249 -0.00194 -0.00093 0.00054 0.00243 0.00460 0.00685 0.00892 0.01051 0.01129 0.01097 0.00934 \
0.00626 0.00178 -0.00392 -0.01044 -0.01724 -0.02362 -0.02880 -0.03199 -0.03245 -0.02961 -0.02318 \
-0.01318 0.00000 0.01554 0.03225 0.04861 0.06289 0.07330 0.07812 0.07594 0.06581 0.04743 0.02122 \
-0.01153 -0.04872 -0.08749 -0.12437 -0.15546 -0.17679 -0.18455 -0.17543 -0.14696 -0.09777 -0.02777 \
0.06172 0.16795 0.28686 0.41325 0.54111 0.66400 0.77543 0.86935 0.94052 0.98491 1.00000 0.98491 \
0.94052 0.86935 0.77543 0.66400 0.54111 0.41325 0.28686 0.16795 0.06172 -0.02777 -0.09777 -0.14696 \
-0.17543 -0.18455 -0.17679 -0.15546 -0.12437 -0.08749 -0.04872 -0.01153 0.02122 0.04743 0.06581 \
0.07594 0.07812 0.07330 0.06289 0.04861 0.03225 0.01554 0.00000 -0.01318 -0.02318 -0.02961 \
-0.03245 -0.03199 -0.02880 -0.02362 -0.01724 -0.01044 -0.00392 0.00178 0.00626 0.00934 0.01097 \
0.01129 0.01051 0.00892 0.00685 0.00460 0.00243 0.00054 -0.00093 -0.00194 -0.00249 -0.00262 \
-0.00245 -0.00206 -0.00156 -0.00105 -0.00060 -0.00024
; Use this to deliberately create aliasing:
;f 1010 0 128 7 0 53 1 22 0 53
</CsScore>
</CsoundSynthesizer>
