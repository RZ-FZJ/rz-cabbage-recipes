import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

n=int(input("Fractal depth? "))
# a=float(input("Scaling factor a? "))
a_init=2.0

nn=2**n

def powf(k,a):
  amp=1.0
  for i in range(1,n+1):
    amp=amp*np.cos(np.pi*k/(a**i))
  return amp**2

def update(val):
    a=fract_slider.val
#    print(a)
    line.set_ydata(powf(f,a))
    fig.canvas.draw_idle()

f=np.arange(1,nn+1)

# print(f)
# print(amp)

fig,ax=plt.subplots()
# ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylim(3.e-10,3.0)
line,=ax.plot(f,powf(f,a_init),'*')
fig.subplots_adjust(left=0.25)
axfract = fig.add_axes([0.1, 0.25, 0.0225, 0.63])
fract_slider = Slider(
    ax=axfract,
    label='Fractal factor',
    valmin=2.,
    valmax=10.,
    valinit=a_init,
    orientation="vertical"
)
fract_slider.on_changed(update)

plt.show()
