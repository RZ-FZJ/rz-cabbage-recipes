; Agenda:
; - aftertouch control (nonlin or temp?)

<Cabbage>
#define STYLE1 fontColour("white"), textColour("white")

form caption("Langevin") size(416, 310), guiMode("queue"), pluginId("lvin"), colour(64, 96, 160), fontColour(240, 240, 240), $STYLE1
; Larger form if csound output is shown:
; form caption("Langevin") size(416, 700), guiMode("queue"), pluginId("lvin"), colour(64, 96, 160), fontColour(240, 240, 240), $STYLE1

rslider bounds(14, 14, 70, 70), channel("impact"), range(0, 1, 1, 0.5), text("Impact"), $STYLE1
rslider bounds(14, 94, 70, 70), channel("attack"), range(0.001, 1, 0.02, 0.5, 0.001), text("Attack"), $STYLE1

rslider bounds(84, 14, 70, 70), channel("fric"), range(0, 100, 0, 0.3), text("Damping"), $STYLE1
rslider bounds(84, 94, 70, 70), channel("fricr"), range(0, 1000, 10, 0.2, 0.001), text("D Release"), $STYLE1 fontColour(255, 255, 255, 255) textColour(255, 255, 255, 255)

rslider bounds(154, 14, 70, 70), channel("temp"), range(0, 300, 0, 0.2, 0.001), text("Temperature"), $STYLE1 fontColour(255, 255, 255, 255) textColour(255, 255, 255, 255)
rslider bounds(152, 94, 70, 70), channel("tempr"), range(0, 10, 0.5, 1, 0.001), text("T Release"), $STYLE1 fontColour(255, 255, 255, 255) textColour(255, 255, 255, 255)

rslider bounds(224, 14, 70, 70), channel("nlin"), range(0, 100, 0, 0.5), text("Nonlinearity"), $STYLE1 fontColour(255, 255, 255, 255) textColour(255, 255, 255, 255)

button bounds(234, 100, 50, 20), channel("stereo"), text("Stereo"), colour:1("red"), $STYLE1
button bounds(234, 134, 50, 20), channel("invert"), text("Invert"), colour:1("red"), $STYLE1

rslider bounds(300, 86, 70, 80), channel("volume"), range(0.01, 10, 1, 0.5, 0.001), text("Volume"), $STYLE1
vslider bounds(372, 82, 40, 80), channel("vumeter"), range(0, 2, 1, 0.5, 0.001), text("VU"), colour(0, 0, 0, 0),  $STYLE1

presetbutton bounds(304, 16, 100, 38) channel("presetChannel"), presetNameAsText(1) userFolder("P-Langevin", "*.psts"), textColour(255), highlightedItemColour(10, 147, 210) highlightedTextColour(255, 255, 255)

keyboard bounds(14, 190, 390, 95)

; csoundoutput bounds(14, 295, 390, 390) channel("csout")

</Cabbage>

<CsoundSynthesizer>

<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>

<CsInstruments>

; Initialize the global variables. 
ksmps = 32
nchnls = 2
0dbfs = 1

; 'Buses' for collecting voices:
gaLeft init 0
gaRight init 0

opcode verlet, ak, kkkikkii
; This opcode does the Verlet calculation. It also handles the attack phase.
; But the release handling is left to the caller.
  ka, kb, kc, ik, knlin, kaa, iattlim, invert xin

  setksmps 1

  idelta = 1/sr
  idp2 = idelta*idelta

  apos0 init 0
  apos1 init 1e-7
  aposl init 1e-7

  katt init 1
  if (katt>0) then
    ka = kaa
    kb = 0
  endif

  aeta random -1.7320508, 1.7320508
  if invert==0 then
    afor = - ik * ( 1 + knlin*apos1*apos1 ) * apos1
  else
    afor = - ik * ( apos1*apos1 - knlin ) * apos1
  endif
  apos = apos1 + ka*(apos1-apos0) + kc*(afor+kb*aeta)
  apos0 = apos1
  apos1 = apos

  krms = rms(apos)
  if (katt>0) then
;   printsk "A: %g %g %g %g %g \n", timeinsts(), ka, kb, kc, k(apos1)
    if (abs(k(apos-apos0))>iattlim) then
      katt = 0
      printsk "Attack phase: %.1f ms\n", 1000.0*timeinsts()
    endif
  else
;   printks "   %g %g %g %g %g \n", 0.1, timeinsts(), ka, kb, kc, k(apos1)
  endif

  xout apos, katt

endop


instr 1

  xtratim 10
  
  SPresetFile, kTrig cabbageGetValue "presetChannel"
  printf "Preset Name: %s\n", kTrig, SPresetFile
  
  iimpact chnget "impact"
  iattt chnget "attack"
  izeta chnget "fric"
  izetar chnget "fricr"
  ktemp0 chnget "temp"
  itempr chnget "tempr"
  knlin chnget "nlin"
  istereo chnget "stereo"
  invert chnget "invert"

  ktempe expsegr 1, 10, 1, itempr, 1e-10
  iom = 6.28318531*p4 
  ik = iom*iom
  ktemp0 = ktemp0 * ik
  idelta = 1/sr
  idp2 = idelta*idelta
  iattz = 15.42/iattt
  print p5
  iattlim = 0.5*iimpact*iom*idelta*p5
  ktemp = ktempe*ktemp0
  if (release()==0) then
    kzeta = izeta
  else
    kzeta = izeta+izetar
  endif
  kzd = kzeta*idelta
  ka = exp(-kzd)
  iaa = exp(iattz*idelta)
  kb = sqrt(2*kzeta*ktemp*sr)
  if (abs(kzd)>1e-6) then
    kc = (1-ka)/(sr*kzeta)
  else
    kc = idp2
  endif
  if (release()==0) then
    kaa = iaa
  else
    kaa = ka
  endif
  koff init 0
  azero init 0
 
  if (koff==0) then
    aposl, katt verlet ka, kb, kc, ik, knlin, kaa, iattlim, invert
    krms = rms(aposl)
;   printks "RMS: %.7f\n", 0.1, krms
    if ((katt==0)&&(krms<1e-6)) then
      koff = 1
    endif
    if (invert==0) then
      if (istereo==0) then
        gaLeft = gaLeft + aposl
        gaRight = gaRight + aposl
      else
        aposr, katt verlet ka, kb, kc, ik, knlin, kaa, iattlim, invert
        gaLeft = gaLeft + aposl
        gaRight = gaRight + aposr
      endif
    else
;     For 'invert' DC has to be blocked.
      if (istereo==0) then
        gaLeft = gaLeft + dcblock(aposl)
        gaRight = gaRight + dcblock(aposl)
      else
        aposr, katt verlet ka, kb, kc, ik, knlin, kaa, iattlim, invert
        gaLeft = gaLeft + dcblock(aposl)
        gaRight = gaRight + dcblock(aposr)
      endif
    endif
  endif

endin


instr 110

  kVolume chnget "volume"
  invert chnget "invert"

  aLeft = kVolume*gaLeft
  aRight = kVolume*gaRight
; protects your ears:
  aLeftc limit aLeft, -10, +10
  aRightc limit aRight, -10, +10
  outs aLeftc, aRightc

  ktrig metro 25
  kVU max_k max(aLeft,aRight), ktrig, 1
  if (ktrig==1) then
    cabbageSetValue "vumeter", kVU
    if (kVU>1) then
      cabbageSet 1, "vumeter", "trackerColour(255,0,0)"
    else
      cabbageSet 1, "vumeter", "trackerColour(0,255,0)"
    endif
  endif

  clear gaLeft
  clear gaRight

endin
  
</CsInstruments>

<CsScore>
i110 0 z
</CsScore>

</CsoundSynthesizer>
