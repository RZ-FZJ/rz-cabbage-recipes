 % !TeX spellcheck = en_GB
 \documentclass[12pt,a4paper]{article}

\usepackage{environ,amsmath}
\usepackage[
colorlinks=true,
urlcolor=blue,
linkcolor=green
]{hyperref}

\textwidth 160mm
\textheight 247mm
\hoffset -15mm
\voffset -20mm
\parindent=0pt
\parskip=6pt
\sloppy

\def\bb{\rule{24pt}{8.2pt}}
\def\bc{\rule{5.875pt}{7.333pt}}
\def\em{\bf}
\def\d{{\mathrm d}}
\def\eq#1{(\ref{#1})}
\def\vec#1{\mathbf{#1}}
\def\note#1{\bb~{\sf #1}}
\def\unit#1{\,{\rm #1}}

\NewEnviron{eqs}{%
	\begin{equation}
		\begin{split}
			\BODY
		\end{split}
\end{equation}}
\NewEnviron{eqm}{%
	\begin{multline}
		\BODY
\end{multline}}

\def\R#1{{\color{red} #1}}
\def\G#1{{\color{green} #1}}
\def\B#1{{\color{blue} #1}}
\def\Y#1{{\color[rgb]{0.8,0.5,0} #1}}

\def\d{{\rm d}}
\def\e{{\rm e}}
\def\i{{\rm i}}
\def\AArez{{\,\mbox{\AA}^{-1}}}
\def\Ar{{\AA$^{-1}$}}
\def\({\left(}
\def\){\right)}
\def\<{\langle}
\def\>{\rangle}
\def\s#1{\hspace{#1cm}}
\def\bb{\rule{24pt}{8.2pt}}
\def\note#1{\bb~{\sf #1}}
\def\eq#1{(\ref{#1})}
\def\Ref#1{ref.~\citenum{#1}}

\begin{document}

\begin{center}
	{\LARGE\bf Langevin}\\[5mm]
	{\bf Version of \today}\\[10mm]
	{\it Reiner Zorn}\\[10mm]
\end{center}

\section{Background}
This instrument is named after Paul Langevin (1872--1946) who devised a stochastic differential equation~\cite{wiki:Langevin} which, among other purposes, can be used to describe the Brownian motion of a particle in a potential:
\begin{equation}
	m { \d^2 x \over \d t^2} = -F(x) - \zeta { \d x \over \d t} + \eta(t) \,.
\end{equation}
$m$ is the mass of the particle, $F(x)$ is the force on the particle. In the simplest case this is a harmonic force $F(x)=-kx$. $\zeta$ is a friction coefficient. Up to the second term on the right hand side, this is the equation of a damped harmonic oscillator generating a sine wave with an exponential envelope. The additional ingredient is a {\em random force} $\eta(t)$ whose root-mean-square value is related to $\zeta$ and the system temperature $T$ by
\begin{equation}\label{fdt}
	\< \eta^2 \> = 2 \zeta k_{\rm B} T \,.
\end{equation}

In this instrument the Langevin equation of an anharmonic oscillator is simulated by physical modelling.
For that purpose, a modification of the Verlet algorithm allowing for a velocity-dependent friction force has been used.~\cite{Pa88}
The output of the instrument is the calculated $x(t)$.

\section{Controls}
The parameter $k$ is set from the MIDI note frequency $f$, using the relation ${\omega_0}^2 = k$. Note that $\omega_0=2\pi f$ only in the undamped/harmonic case. So the instrument may get out of tune for high setting of the anharmonicity/non-linearity.

For percussive sounds the instrument is started by transferring an amount of energy controlled by {\sf Impact}. The energy increase is effected by setting $\zeta$ to a negative value, a `negative friction'. {\sf Attack} controls the time over which this energy loading takes place. Larger values result in a slower attack.

{\sf Damping} controls parameter $\zeta$. Thus, larger values mean a faster decay here.

The release phase of the instrument is realised by increasing $\zeta$ by the value controlled by {\sf D~Release} and `cooling' (see below).

{\sf Temperature} controls the system temperature $T$ which in turn determines the amplitude of the random force. This is somewhat like a noise added, but does not directly add up to the output $x(t)$ but only filtered by the Langevin equation. Note that because of equation~\eq{fdt}, known as the fluctuation-dissipation theorem, there will be no effect of $T>0$ unless $\zeta>0$ so there is some damping.

Due to the random force the instrument starts playing spontaneously even without `impact'. This can be used as an ultra-smooth attack.

Because the thermal fluctuation keeps the instrument playing even through the release phase, the temperature will be ramped down over a time controlled by {\sf T~Release} during the release phase.

{\sf Nonlinearity} introduces an anharmonicity by adding a third order term to the force, $ F(x) = -kx -k'x^3 $. This introduces harmonics as a distortion would do but also makes the frequency amplitude-dependent. So, a `chirped' sound emerges which reaches the MIDI note pitch only for long times. In combination with the random force, results may be difficult to predict. In order to avoid too loud sounds, volume should be reduced. (In addition there is a built-in clipping at +12~dB to protect your ears.)

The {\sf Stereo} button makes the left and right channels originate from separate simulations. If damping and temperature are turned on, this results in the two simulations diverging and creating a kind of Haas effect.

The most dangerous button, {\sf Invert}, changes the anharmonic force to $ F(x) = +kx -k'x^3 $. This corresponds to a `double-well' potential $ U(x) = k' x^4/4 - k x^2/2 $. The sound results from both oscillations within the wells and jumping between both. Because the potential minimum is not at $x=0$ anymore, a DC blocker is applied before outputting the signal.

For the most basic settings the energy in the system will not get so high that the output $x(t)$ is larger than one. Nevertheless, this may happen for even simple presets. To avoid clipping, {\sf Volume} can be reduced according to the readings of the VU meter.

\section{Known bugs and problems}
Because it is not immediately clear from the settings how long the release phase will be, an extra time of 10 seconds ({\tt xtratim 10}) is applied. Unfortunately, the mechanism of Csound to kill the running instance of an instrument itself without killing others started by polyphony, is rather complicated. To avoid CPU overload, the main calculation running in the opcode {\tt verlet} at ksmps=1 is stopped as soon as r.m.s.\ falls below $-120$~dB. Nevertheless, the instance of the instrument continues playing a zero value until 10 seconds are over. This may lead to an accumulation of processes and unnecessary CPU load for fast playing.


\section{Additional features to be implemented}
It would be cool to control either the temperature or non-linearity by aftertouch.

\bibliographystyle{../rz}
\bibliography{../csound}

\end{document}
