; Agenda:
; - turnoff
; - VU meter, gain knob
; - brush up interface
; - 'setksmps' opcode
; - aftertouch control

<Cabbage>
#define STYLE1 fontColour("white"), textColour("white")
form caption("Langevin") size(540, 300), guiMode("queue"), pluginId("lvin"), colour(64, 96, 160), fontColour(240, 240, 240), $STYLE1
rslider bounds(14, 14, 70, 70), channel("impact"), range(0, 1, 1, 0.5), text("Impact"), $STYLE1
rslider bounds(14, 94, 70, 70), channel("attack"), range(0.001, 0.1, 0.001, 0.5), text("Attack"), $STYLE1
rslider bounds(84, 14, 70, 70), channel("fric"), range(0, 100, 0, 0.3), text("Damping"), $STYLE1
rslider bounds(154, 14, 70, 70), channel("fricr"), range(0, 1000, 10, 0.2), text("D Release"), $STYLE1
rslider bounds(224, 14, 70, 70), channel("temp"), range(0, 1000, 0, 0.2), text("Temperature"), $STYLE1
rslider bounds(294, 14, 70, 70), channel("tempr"), range(0, 10, 0.5), text("T Release"), $STYLE1
rslider bounds(364, 14, 70, 70), channel("nlin"), range(0, 1, 0), text("Nonlinearity"), $STYLE1
button bounds(434, 14, 70, 20), channel("stereo"), text("Stereo"), colour:1("red"), $STYLE1
keyboard bounds(8, 178, 381, 95)
</Cabbage>

<CsoundSynthesizer>

<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>

<CsInstruments>

; Initialize the global variables. 
ksmps = 1
nchnls = 2
0dbfs = 1


instr 1

  xtratim 10 ; should be replaced by dynamic mechanism
  print p1

  iimpact chnget "impact"
  iattt chnget "attack"
  izeta chnget "fric"
  izetar chnget "fricr"
  ktemp0 chnget "temp"
  ktemp0 = ktemp0 * p4*p4
  itempr chnget "tempr"
  ktempe expsegr 1, 10, 1, itempr, 1e-10
  kxnlin chnget "nlin"
  knlin = 10^kxnlin - 1
  iom = 6.28318531*p4 
  ik = iom*iom
  istereo chnget "stereo"
  katt init 1

  
; boundary conditions:
  apos0l init 0
  apos1l init 1e-7
  aposl init 1e-7
  apos0r init 0
  apos1r init 1e-7
  aposr init 1e-7

; coefficient initialisation:
  idelta = 1/sr
  idp2 = idelta*idelta
  iattz = 15.42/iattt
  iattlim = 0.5*iimpact*iom/sr
  if (release()==0) then
    kzeta = izeta
  else
    kzeta = izeta+izetar
    katt = 0
  endif
  if (katt>0) then
    kzeta = -iattz
  endif
  print iattz
  ktemp = ktempe*ktemp0
  kzd = kzeta*idelta
  ka = exp(-kzd)
  kb = sqrt(max(2*kzeta*ktemp/idelta,0.0))
  if (abs(kzd)>1e-6) then
    kc = (1-ka)*idelta/kzeta
  else
    kc = idp2
  endif

; Verlet cycle:
  aetal random -1.7320508, 1.7320508
  aforl = - ik * ( 1 - knlin + knlin*apos1l*apos1l ) * apos1l
  aposl = apos1l + ka*(apos1l-apos0l) + kc*(aforl+kb*aetal)
  apos0l = apos1l
  apos1l = aposl
  krms = rms(aposl)
  if (katt>0) then
;   printsk "A: %g %g %g %g %g \n", timeinsts(), ka, kb, kc, k(apos1l)
    if (abs(k(aposl-apos0l))>iattlim) then
      katt = 0
      printsk "Attack phase: %.1f ms\n", 1000.0*timeinsts()
    endif
  else
;   printks "   %g %g %g %g %g \n", 0.1, timeinsts(), ka, kb, kc, k(apos1l)
    printks "RMS: %.7f\n", 0.1, krms
    if krms<1e-6 then
      turnoff2 1, 0, 0
    endif
  endif

;  ktrig metro 10
;  kmax max_k aposl, ktrig, 1
;  printks "%g %g\n", 0.1, rms(aposl), kmax
;
;  if (istereo==1) then
;    aetar random -1.7320508, 1.7320508
;    avelr = avelr + idelta2*aforr + kb*aetar
;    aposr = aposr + kc*avelr
;    apos2r = aposr*aposr
;    aforr = - ( ik + knlin*apos2r ) * aposr
;    if (timeinstk()<=iattc) then
;      aforr = aforr + iattf
;    endif
;    avelr = ka*avelr + kb*aetar + idelta2*aforr
;  endif
;  
  if (istereo==1) then
    aposll limit aposl, -10, +10
    aposrl limit aposr, -10, +10
    outs aposl, aposr
  else
    aposll limit aposl, -10, +10
    outs aposll, aposll
  endif

endin

</CsInstruments>

<CsScore>
</CsScore>

</CsoundSynthesizer>
