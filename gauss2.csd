; Gaussian Random Field

; To do:
; + handle change of iNpart under freeze
; + mod wheel curve

<Cabbage>

form caption("Gaussian Random Field") size(512, 490), colour(58, 110, 182), pluginId("grf1"), guiMode("queue")
label bounds(412, 480, 100, 10), text("Gauss_1")

; first row:

rslider bounds(12, 14, 70, 70), channel("vexp"), range(0, 2, 1, 0.5, 0.01), text("VelExp")

vslider bounds(132, 10, 30, 70), channel("vib"), range(0, 1, 0, 0.3, 0.001)
button bounds(102, 14, 22, 22), channel("mvib"), text("M"), colour:1("red")
rslider bounds(96, 42, 34, 34), channel("fvib"), range(0.1, 20, 5, 0.5, 0.01)
label bounds(90, 76, 66, 12), text("Vibrato"), colour(0,0,0,0)

button bounds(170, 48, 22, 22), channel("mfcouple"), text("="), colour:1("red")

vslider bounds(236, 10, 30, 70), channel("trem"), range(0, 5, 0, 0.5, 0.02)
button bounds(206, 14, 22, 22), channel("mtrem"), text("M"), colour:1("red")
rslider bounds(200, 42, 34, 34), channel("ftrem"), range(0.1, 20, 5, 0.5, 0.01)
label bounds(200, 76, 66, 12), text("Tremolo"), colour(0,0,0,0)

rslider bounds(276, 14, 70, 70), channel("pbend"), range(0, 12, 2, 1, 0.01), text("PBendRng")

combobox bounds(390, 14, 100, 25), channelType("string"), channel("comboChannel"), populate("*.snaps"), value("dry-default")
filebutton bounds(390, 50, 25, 25), channel("save"), text("+"), mode("named snapshot"), populate("*.snaps")
filebutton bounds(420, 50, 25, 25), channel("remove"), text("-"), mode("remove preset"), populate("*.snaps")

; second row:

vslider bounds(12, 94, 30, 70), channel("npart"), range(1, 4096, 1024, 0.5), text("N")
button bounds(50, 94, 30, 22), channel("lin"), text("lin"), colour:1("red"), radioGroup("linlog"), value(0)
button bounds(50, 120, 30, 22), channel("log"), text("log"), colour:1("red"), radioGroup("linlog"), value(1)
button bounds(90, 94, 50, 22), channel("equi"), text("equi"), colour:1("red"), radioGroup("sampling"), value(0)
button bounds(90, 120, 50, 22), channel("random"), text("random"), colour:1("red"), radioGroup("sampling"), value(1)
button bounds(90, 146, 50, 22), channel("freeze"), text("freeze"), colour:1("red"), radioGroup("sampling"), value(0)
rslider bounds(152, 94, 70, 70), channel("spread"), range(0, 10, 5, 0.5, 0.01), text("Spread")
rslider bounds(252, 94, 34, 34), channel("spini"), range(0, 10, 5, 0.5, 0.01), text("Ini")
rslider bounds(252, 134, 34, 34), channel("sprel"), range(0, 1, 0, 0.5), text("Rel")
button bounds(222, 94, 22, 22), channel("mspread"), text("M"), colour:1("red")
rslider bounds(292, 94, 70, 70), channel("width"), range(0.001, 10, 5, 0.5, 0.01), text("Width")
rslider bounds(392, 94, 34, 34), channel("wiini"), range(0.001, 10, 5, 0.5, 0.01), text("Ini")
rslider bounds(392, 134, 34, 34), channel("wirel"), range(0, 1, 0, 0.5), text("Rel")
button bounds(362, 94, 22, 22), channel("mwidth"), text("M"), colour:1("red")
rslider bounds(432, 94, 70, 70), channel("nexp"), range(-2, 2, 0), text("Noise Exp")

; third row:

;gentable bounds(12,174,130,70), channel("gt"), tableNumber(5), active(1), ampRange(-1,1,5,0.01), text("Clip")
;rslider bounds(82, 174, 70, 70), channel("soft"), range(0, 1, 0.5), text("Soft")
;rslider bounds(12, 174, 70, 70), channel("lim"), range(0.01, 5, 1), text("Limit")
combobox bounds(12, 184, 90, 25), channel("wavef"), items("sine","saw","square","triangle"), value(1)
label bounds(12, 220, 90, 14), text("Waveform"), colour(0,0,0,0)
filebutton bounds(115, 184, 25, 25), channel("saver"), mode("save"), text("S"), populate("*.rand")
filebutton bounds(115, 214, 25, 25), channel("loadr"), mode("file"), text("L"), populate("*.rand")
rslider bounds(152, 174, 70, 70), channel("att"), range(0.01, 1, 0.02, 0.5, 0.001), text("Attack")
vslider bounds(215, 174, 30, 70), channel("xatt"), range(0, 1, 0, 1, 0.01)
rslider bounds(247, 174, 70, 70), channel("dec"), range(0.001, 1, 0.001, 1), text("Decay")
vslider bounds(310, 174, 30, 70), channel("xdec"), range(0, 1, 0, 1, 0.01)
rslider bounds(337, 174, 70, 70), channel("sus"), range(0, 1, 1, 0.5, 0.01), text("Sustain")
rslider bounds(405, 174, 70, 70), channel("rel"), range(0, 1, 0.1, 1, 0.01), text("Release")

; fourth row:

rslider bounds(12, 254, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(82, 254, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(152, 254, 70, 70), channel("wet"), range(0, 1, 0, 1, .01), text("Dry/Wet")
button bounds(222, 254, 22, 22), channel("rreset"), latched(0), text("R"), colour:1("red")
rslider bounds(405, 254, 70, 70), channel("volume"), range(0, 2, 0.3, 0.5, 0.001), text("Volume")
vslider bounds(470, 249, 30, 75), channel("vumeter"), range(0, 2, 1, 0.5), text("VU"), colour(0,0,0,0)

; soft keyboard:

keyboard bounds(14, 360, 484, 95), value(36)

</Cabbage>

<CsoundSynthesizer>

<CsOptions>
; -n -d -M0 -m0d -Q2 ; LEDs funktionieren
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5 ; LEDs funktionieren nicht
</CsOptions>

<CsInstruments>

massign 0,0
massign 1,1

#define MPART #4096#

; Initialize the global variables.
ksmps = 32
nchnls = 2
0dbfs = 1

gaDry init 0

giRand ftgen 0, 0, $MPART, 7, 0, $MPART, 0


instr 1

iBase = p4
iAmp = p5
iVexp chnget "vexp"
iPbrange chnget "pbend"
kVib chnget "vib"
kFvib chnget "fvib"
kTrem chnget "trem"
kFtrem chnget "ftrem"
iNpart chnget "npart"
iSlin chnget "lin"
iSlog chnget "log"
iSequi chnget "equi"
iSrandom chnget "random"
iSfreeze chnget "freeze"
iSprel chnget "sprel"
if (iSprel>0) then
  iSpfin chnget "spread"
else
  kSpread chnget "spread"
endif
iSpini chnget "spini"
iWirel chnget "wirel"
if (iWirel>0) then
  iWifin chnget "width"
else
  kWidth chnget "width"
endif
iWiini chnget "wiini"
kNexp chnget "nexp"
iWavef chnget "wavef"
iLim chnget "lim"
iSoft chnget "soft"
iAtt chnget "att"
iXatt chnget "xatt"
iDec chnget "dec"
iXdec chnget "xdec"
iSus chnget "sus"
iRel chnget "rel"
kVolume chnget "volume"

if iSrandom==1 then
  giRand ftgen 0, 0, $MPART, 21, 1
endif
if iSequi==1 then
  giRand ftgen 0, 0, $MPART, 7, 0, iNpart-1, 1, 1, 1
endif
iFreqs ftgentmp 0, 0, $MPART, 7, 0, $MPART, 0
iAmps ftgentmp 0, 0, $MPART, 7, 0, $MPART, 0

if iWavef==1 then
  iwvfn=-1
else
  iwvfn=iWavef+8
endif

; Modify velocity curve (gamma correction):
iAmp = iAmp^iVexp

; Appply pitch bend:
kpb pchbend 0,iPbrange
kBase = iBase * 1.059463^kpb

; Calculate vibrato and tremolo:
kVibo lfo kVib, kFvib
kTremo lfo kTrem, kFtrem

; Automate spread/width if desired:
if (iSprel>0) then
  kSpread linseg iSpini, iSprel, iSpfin
endif
if (iWirel>0) then
  kWidth linseg iWiini, iWirel, iWifin
endif

; Calculate partials:
if ( changed(kSpread)==1 || changed(kWidth)==1 || changed(kNexp)==1 || timeinstk()==1 ) then
  kIndex = 0
  kLfreq0 = 1.442695*log(kBase)
  if kLfreq0<9.3 then
    if kSpread>2*(kLfreq0-4.3) then
      kLfreq1 = 4.3
      kLfreq2 = kSpread+4.3
    else
      kLfreq1 = kLfreq0-kSpread/2
      kLfreq2 = kLfreq0+kSpread/2
    endif
  else
    if kSpread>2*(14.3-kLfreq0) then
      kLfreq1 = 14.3-kSpread
      kLfreq2 = 14.3
    else
      kLfreq1 = kLfreq0-kSpread/2
      kLfreq2 = kLfreq0+kSpread/2
    endif
  endif
  kSumamp = 0
  kSumm1 = 0
  kSumm2 = 0
  loop:
    kRand tab kIndex, giRand
    if iSlog==1 then
      kLfreq = kLfreq1 + (kLfreq2-kLfreq1)*kRand
      kFreq = 2^kLfreq
    else
      kFreq1 = 2^kLfreq1
      kFreq = kFreq1 + (2^kLfreq2-kFreq1)*kRand
      kLfreq = 1.442695*log(kFreq)
    endif
    tabw kFreq, kIndex, iFreqs
    if kFreq<sr/2 then
      if iSlog==1 then
;       default: propto sqrt(kFreq) to get white noise
        kAmp0 = (kFreq)^(0.5*kNexp+0.5) * exp(-0.5*((kLfreq-kLfreq0)/kWidth)^2)
      else
        kAmp0 = (kFreq)^(0.5*kNexp) * exp(-0.5*((kLfreq-kLfreq0)/kWidth)^2)
      endif
      kSumamp = kSumamp + kAmp0^2
;      kSumm1 = kSumm1 + kAmp0^2 * kLfreq
;      kSumm2 = kSumm2 + kAmp0^2 * kLfreq^2
      tabw kAmp0, kIndex, iAmps
    else
      tabw 0, kIndex, iAmps
    endif
  loop_le kIndex, 1, iNpart, loop
;  kLcent = kSumm1/kSumamp
;  kCent = 2^kLcent
;  kCoff = kLcent-kLfreq0
;  kRwidth = sqrt( max( 0, kSumm2*kSumamp - kSumm1^2 ) ) / kSumamp
;  printf "Centroid: %.2f Hz,  Offset: %2.2f octaves,  Effective width: %2.2f octaves\n", 1, kCent, kCoff, kRwidth
endif

; Generate sound:

; no clipping:
asig adsynt kVolume*iAmp/sqrt(kSumamp)*(1.122018^kTremo), 1.059463^kVibo, iwvfn, iFreqs, iAmps, iNpart, 2

; table-based clipping (too complex):
;aIndex adsynt2 0.2/sqrt(kSumamp), 1, -1, iFreqs, iAmps, iNpart, 2
;asig tablei aIndex+0.5, 5, 1
;asig = iAmp*(1.122018^kTremo) * asig

; clipping by Csound's clip:
;asig0 adsynt2 1/sqrt(kSumamp), 1, -1, iFreqs, iAmps, iNpart, 2
;asig1 clip asig0, 0, iLim, iSoft
;asig = kVolume*iAmp*asig1*(1.122018^kTremo)

; hardest clip possible:
;asig0 adsynt2 1, 1, -1, iFreqs, iAmps, iNpart, 2
;asig = signum(asig0)

; Calculate envelope:
iAtt=iAtt/((iBase/27.5)^iXatt)
iDec=max(0.02,iDec/((iBase/440)^iXdec))
iPhase2 = 10-iAtt
iShape2 = (iAtt-10)/iDec
aEnv transegr 0, iAtt, 0, 1, iPhase2, iShape2, iSus, iRel, -10, 0

; If used with reverb:
gaDry = gaDry + asig*aEnv

; If used without reverb:
; outs asig*kEnv, asig*kEnv

endin


; This instrument deals with some 'parallel' tasks:
; * setting the sliders from mod wheel
; * coupling of sliders
; * loading/saving the random pattern

instr 2

kMvib chnget "mvib"
kMtrem chnget "mtrem"
kFcouple chnget "mfcouple"
kFvib chnget "fvib"
kFtrem chnget "ftrem"
kSpread chnget "spread"
kMspread chnget "mspread"
kWidth chnget "width"
kMwidth chnget "mwidth"

if (kMvib!=0) then
  kVibm ctrl7 1, 1, 0, 1
  cabbageSetValue "vib", kVibm, changed(kVibm)
endif

if (kMtrem!=0) then
  kTremm ctrl7 1, 1, 0, 5
  cabbageSetValue "trem", kTremm, changed(kTremm)
endif

if (kFcouple!=0) then
  cabbageSetValue "ftrem", kFvib, changed(kFvib)
  cabbageSetValue "fvib", kFtrem, changed(kFtrem)
endif

if (kMspread!=0) then
  kSpreadm ctrl7 1, 1, 0, 10
  cabbageSetValue "spread", kSpreadm, changed(kSpreadm)
endif

if (kMwidth!=0) then
  kWidthm ctrl7 1, 1, 0.001, 10
  cabbageSetValue "width", kWidthm, changed(kWidthm)
endif

Sfilename, kSaver cabbageGetValue "saver"
ftsavek Sfilename, kSaver, 1, giRand

Sfilename, kLoadr cabbageGetValue "loadr"
ftloadk Sfilename, kLoadr, 1, giRand

endin


; This instrument adds the reverb and controls the VU meter:

instr 10

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"
kRreset chnget "rreset"

aLrev, aRrev freeverb gaDry, gaDry, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaDry + kWet*aLrev
aRtot = kDry*gaDry + kWet*aRrev

outs aLtot, aRtot

ktrig metro 25
kVU max_k gaDry, ktrig, 1
if (ktrig==1) then
  cabbageSetValue "vumeter", kVU
  if (kVU>1) then
    cabbageSet 1, "vumeter", "trackerColour(255,0,0)"
  else
    cabbageSet 1, "vumeter", "trackerColour(0,255,0)"
  endif
endif

; The following code flags NaN/Inf and gives the user possibility to restart the instrument:
kAvwet max_k aLrev+aRrev, ktrig, 4
if (qnan(kAvwet)>0)||(qinf(kAvwet)>0) then
  cabbageSet  1, "rreset", "colour:0", 128,0,0
endif

if (changed(kRreset)==1) then
  if (kRreset==1) then
    cabbageSet  1, "rreset", "colour:0", 0,0,0
    event "i", 10, 0, 1000000
    turnoff
  endif
endif

clear gaDry

endin


; Instrument 20 establishes the MIDI interface:

instr 20

kstatus,kchan,kdata1,kdata2 midiin

iNsliders = 16
Ssliders[] fillarray "npart", "spread", "width", "nexp", "spini", "sprel", "wiini", "wirel", "att", "dec", "sus", "rel", "room", "damp", "wet", "volume"
islstart[] fillarray 1, 0, 0.001, -2, 0, 0, 0.001, 0, 0, 0, 0, 0, 0, 0, 0, 0
islend[] fillarray 4096, 10, 10, 2, 10, 1, 10, 1, 1, 1, 1, 1 , 1, 1, 1, 2
islskew[] fillarray 0.5, 0.5, 0.5, 1, 0.5, 0.5, 0.5, 0.5, 1, 0.5, 0.5, 1, 1, 1, 1, 1
islcc[] fillarray 0, 1, 2, 3, 16, 17, 18, 19, 20, 21, 22, 23, 4, 5, 6, 7

iNbuttons = 6
Sbuttons[] fillarray "lin", "log", "equi", "random", "freeze", "mspread", "mwidth"
iburgrp[] fillarray 1, 1, 2, 2, 2, 0, 0
ibucc[] fillarray 32, 48, 33, 49, 65, 34, 35

iNcboxes = 1
Scboxes[] fillarray "wavef"
icblen[] fillarray 4
icbdcc[] fillarray 61
icbucc[] fillarray 62

kzero init 0
kone init 1

if (kstatus==176) then

; uses channel 7 to avoid clash with keyboard
  if (kchan==7) then

    kindex = 0
    loop_sliders:
      if (kdata1==islcc[kindex]) then
        kvalue = islstart[kindex]+(islend[kindex]-islstart[kindex])*((7.874015748e-3*kdata2)^(1/islskew[kindex]))
        cabbageSetValue Ssliders[kindex], kvalue
      endif
    loop_lt kindex, 1, iNsliders, loop_sliders

    kindex = 0
    loop_buttons:
      if ((kdata1==ibucc[kindex])&&(kdata2>0)) then
        krgrp = iburgrp[kindex]
        if (krgrp>0) then
          cabbageSetValue Sbuttons[kindex], kone
          kindex1 = 0
          loop1:
            if ((krgrp==iburgrp[kindex1])&&(kindex1!=kindex)) then
              cabbageSetValue Sbuttons[kindex1], kzero
            endif
          loop_lt kindex1, 1, iNbuttons, loop1
        else
          kbold chnget Sbuttons[kindex]
          if (kbold==0) then
            cabbageSetValue Sbuttons[kindex], kone
          else
            cabbageSetValue Sbuttons[kindex], kzero
          endif
        endif
      endif
    loop_lt kindex, 1, iNbuttons, loop_buttons

    kindex = 0
    loop_cboxes:
      if ((kdata1==icbdcc[kindex])&&(kdata2>0)) then
        koldsel cabbageGetValue Scboxes[kindex]
        knewsel = koldsel - 1
        if (knewsel<1) then
          knewsel = icblen[kindex]
        endif
        cabbageSetValue Scboxes[kindex], knewsel
      endif
      if ((kdata1==icbucc[kindex])&&(kdata2>0)) then
        koldsel cabbageGetValue Scboxes[kindex]
        knewsel = koldsel + 1
        if (knewsel>icblen[kindex]) then
          knewsel = 1
        endif
        cabbageSetValue Scboxes[kindex], knewsel
      endif
    loop_lt kindex, 1, iNcboxes, loop_cboxes

  endif
endif

endin


</CsInstruments>

<CsScore>
; The initial table for clipping:
;f 5 0 1024 -7 -1 1024 1

; The basic waveforms:
f 10 0 16384 10 1 0.5 0.3 0.25 0.2 0.167 0.143 0.125 .111
f 11 0 16384 10 1 0 0.3 0 0.2 0 0.143 0 0.111
f 12 0 16384 10 1 0 0.111 0 0.04 0 0.002 0 0.001

i2 1 z
i10 0 z
i20 0 z
</CsScore>

</CsoundSynthesizer>
