## Collection of RZ's Cabbage/Csound files

Some small demos are in the subdirectory `Demos`. If you want to check the presets or change them you can download the REAPER files.

### Etude #1
This is a simple instrument based on harmonic partials generated from a recursive rule. The demo is the prelude from BWV 846 played by Etude #1 with additional voices created by other (non-cabbage) plugins.

### Etude #2
This instrument is based on the eigenfrequencies of a rectangular plate. The demo here is completely played by different presets of Etude #2, percussion, bass, pad and lead.

### Etude #3
The basis of this instrument is a pulse sequence which can be varied between a regular trail and a random process. The demo is completely played by this instrument.

### Gauss
This instrument is based on Gaussian random fields. It produces intermediates between noise and tonal sounds.

### Morpheus
Morpheus is an instrument designed for live performance to morph between different control setting depending on the velocity played.

### Anykey
This is a helper plugin which replaces arbitrary keys played in a performance by correct keys from a score.
It is not an instrument by itself but an effect acting on the MIDI messages.
