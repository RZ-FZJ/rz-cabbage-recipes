; Morpheus: morfing of presets via pitch and velocity
; Agenda:
;   morf pitch ?
; Convention: channel "xyzzy" <-> variable iFxyzzy

<Cabbage>
form caption("Morpheus") size(676, 410), colour(58, 110, 182),pluginId("morf")

keyboard bounds(14, 280, 648, 95), value(36)

rslider bounds(12, 14, 70, 70), channel("vexp"), range(0, 2, 1, 0.5, 0.01), text("VelExp")
rslider bounds(82, 14, 70, 70), channel("vib"), text("Vibrato"), range(0, 1, 0, 0.3, 0.001), colour(255, 220, 220)
rslider bounds(152, 14, 70, 70), channel("fvib"), text("V freq"), range(0.1, 20, 5, 0.5, 0.01), colour(255, 220, 220)
rslider bounds(222, 14, 70, 70), channel("trem"), text("Tremolo"), range(0, 5, 0, 0.5, 0.02), colour(255, 220, 220)
rslider bounds(292, 14, 70, 70), channel("ftrem"), text("T freq"), range(0.1, 20, 5, 0.5, 0.01), colour(255, 220, 220)
rslider bounds(362, 14, 70, 70), channel("pbend"), range(0, 12, 2, 1, 0.01), text("PBendRng")
rslider bounds(432, 14, 70, 70), channel("volume"), range(0.01, 10, 1, 0.5), text("Volume"), colour(255, 220, 220)
vslider bounds(500, 9, 30, 75), channel("vumeter"), identChannel("vumeter_id"), range(0, 2, 1, 0.5), text("VU"), colour(0,0,0,0)

button bounds(552, 14, 104, 32), channel("learn_all"), text("All"), value(1), radioGroup(1), colour:1("red")
button bounds(552, 50, 32, 32), channel("learn_l"), text("L"), radioGroup(1), colour:1(224,0,128)
button bounds(588, 50, 32, 32), channel("learn_m"), text("M"), radioGroup(1), colour:1(224,0,128)
button bounds(624, 50, 32, 32), channel("learn_h"), text("H"), radioGroup(1), colour:1(224,0,128)
button bounds(516, 86, 32, 32), channel("learn_f"), text("f"), radioGroup(1), colour:1("red")
button bounds(552, 86, 32, 32), channel("learn_lf"), text("Lf"), radioGroup(1), colour:1("red")
button bounds(588, 86, 32, 32), channel("learn_mf"), text("Mf"), radioGroup(1), colour:1("red")
button bounds(624, 86, 32, 32), channel("learn_hf"), text("Hf"), radioGroup(1), colour:1("red")
button bounds(516, 122, 32, 32), channel("learn_p"), text("p"), radioGroup(1), colour:1("red")
button bounds(552, 122, 32, 32), channel("learn_lp"), text("Lp"), radioGroup(1), colour:1("red")
button bounds(588, 122, 32, 32), channel("learn_mp"), text("Mp"), radioGroup(1), colour:1("red")
button bounds(624, 122, 32, 32), channel("learn_hp"), text("Hp"), radioGroup(1), colour:1("red")
button bounds(552, 158, 104, 32), channel("play"), text("Play"), radioGroup(1), colour:1("green")

combobox bounds(552, 194, 104, 32), channelType("string"), channel("load"), populate("*.mset","Mpresets"), value("")
filebutton bounds(606, 230, 50, 32), channel("save"), text("save"), mode("save"), populate("*.mset","Mpresets")
; filebutton bounds(552, 194, 50, 32), channel("open"), text("open"), mode("file"), populate("*.mset","Mpresets")

rslider bounds(12, 94, 70, 70), channel("wform"), range(0, 12, 0, 1, 2), text("Waveform")
rslider bounds(82, 94, 70, 70), channel("width"), range(0.01, 0.99, 0.5, 1), text("Width"), colour(255, 220, 220)
rslider bounds(152, 94, 70, 70), channel("fmin"), range(20, 20000, 440, 0.5), text("LP min F"), colour(255, 220, 220)
rslider bounds(222, 94, 70, 70), channel("frange"), range(0, 10, 0), text("LP range"), colour(255, 220, 220)
rslider bounds(292, 94, 70, 70), channel("order"), range(0, 8, 4, 1, 1), text("LP order")
rslider bounds(362, 94, 70, 70), channel("fatt"), range(0.01, 1, 0.02, 0.5, 0.001), text("LP attack"), colour(255, 220, 220)
rslider bounds(432, 94, 70, 70), channel("fdec"), range(0, 1, 0.5, 1, .01), text("LP decay"), colour(255, 220, 220)

rslider bounds(12, 174, 70, 70), channel("att"), range(0.01, 1, 0.02, 0.5, 0.001), text("Attack"), colour(255, 220, 220)
rslider bounds(82, 174, 70, 70), channel("dec"), range(0, 1, 0.5, 1, .01), text("Decay"), colour(255, 220, 220)
rslider bounds(152, 174, 70, 70), channel("sus"), range(0, 1, 0.1, 0.5, 0.01), text("Sustain"), colour(255, 220, 220)
rslider bounds(222, 174, 70, 70), channel("rel"), range(0, 5, 0.1, 1, 0.01), text("Release"), colour(255, 220, 220)
rslider bounds(292, 174, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(362, 174, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(432, 174, 70, 70), channel("wet"), range(0, 1, 0.5, 1, .01), text("Dry/Wet")
</Cabbage>
<CsoundSynthesizer>


<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>


<CsInstruments>

; Initialize the global variables:

ksmps = 4
nchnls = 2
0dbfs = 1

gaDry init 0

; Table to store the registers:
giPresets ftgen 0, 0, 1024, 7, 0, 1024, 0
giFixed ftgen 0, 0, 32, -2, 1, 2, 0, 4, 0.9, 0.35, 0.5, 20, 440, 20000


#define SETLIN(CHAN'OFF)#
if (iPlay==1) then
  if (iBase<iFl) then
    iC3 = 1-iVel
    iC0 = iVel
    iPres0 tab_i $OFF, giPresets
    iPres3 tab_i 3+$OFF, giPresets
    iF$CHAN = iPres0*iC0 + iPres3*iC3
  elseif (iBase<iFm) then
    iLfml = log(iFm/iFl)
    iC4 = (1-iVel)*log(p4/iFl)/iLfml
    iC3 = (1-iVel)*log(iFm/p4)/iLfml
    iC1 = iVel*log(p4/iFl)/iLfml
    iC0 = iVel*log(iFm/p4)/iLfml
    iPres0 tab_i $OFF, giPresets
    iPres1 tab_i 1+$OFF, giPresets
    iPres3 tab_i 3+$OFF, giPresets
    iPres4 tab_i 4+$OFF, giPresets
    iF$CHAN = iPres0*iC0 + iPres1*iC1 + iPres3*iC3 + iPres4*iC4
  elseif (iBase<iFh) then
    iLfhm = log(iFh/iFm)
    iC5 = (1-iVel)*log(p4/iFm)/iLfhm
    iC4 = (1-iVel)*log(iFh/p4)/iLfhm
    iC2 = iVel*log(p4/iFm)/iLfhm
    iC1 = iVel*log(iFh/p4)/iLfhm
    iPres0 tab_i 1+$OFF, giPresets
    iPres1 tab_i 2+$OFF, giPresets
    iPres3 tab_i 4+$OFF, giPresets
    iPres4 tab_i 5+$OFF, giPresets
    iF$CHAN = iPres0*iC1 + iPres1*iC2 + iPres3*iC4 + iPres4*iC5
  else
    iC5 = 1-iVel
    iC2 = iVel
    iPres0 tab_i 2+$OFF, giPresets
    iPres3 tab_i 5+$OFF, giPresets
    iF$CHAN = iPres0*iC2 + iPres3*iC5
  endif
  chnset iF$CHAN, "$CHAN"
else
  iF$CHAN chnget "$CHAN"
endif
#

#define SETLOG(CHAN'OFF)#
if (iPlay==1) then
  if (iBase<iFl) then
    iC3 = 1-iVel
    iC0 = iVel
    iPres0 tab_i $OFF, giPresets
    iPres3 tab_i 3+$OFF, giPresets
    iF$CHAN = iPres0^iC0 * iPres3^iC3
  elseif (iBase<iFm) then
    iLfml = log(iFm/iFl)
    iC4 = (1-iVel)*log(p4/iFl)/iLfml
    iC3 = (1-iVel)*log(iFm/p4)/iLfml
    iC1 = iVel*log(p4/iFl)/iLfml
    iC0 = iVel*log(iFm/p4)/iLfml
    iPres0 tab_i $OFF, giPresets
    iPres1 tab_i 1+$OFF, giPresets
    iPres3 tab_i 3+$OFF, giPresets
    iPres4 tab_i 4+$OFF, giPresets
    iF$CHAN = iPres0^iC0 * iPres1^iC1 * iPres3^iC3 * iPres4^iC4
  elseif (iBase<iFh) then
    iLfhm = log(iFh/iFm)
    iC5 = (1-iVel)*log(p4/iFm)/iLfhm
    iC4 = (1-iVel)*log(iFh/p4)/iLfhm
    iC2 = iVel*log(p4/iFm)/iLfhm
    iC1 = iVel*log(iFh/p4)/iLfhm
    iPres0 tab_i 1+$OFF, giPresets
    iPres1 tab_i 2+$OFF, giPresets
    iPres3 tab_i 4+$OFF, giPresets
    iPres4 tab_i 5+$OFF, giPresets
    iF$CHAN = iPres0^iC1 * iPres1^iC2 * iPres3^iC4 * iPres4^iC5
  else
    iC5 = 1-iVel
    iC2 = iVel
    iPres0 tab_i 2+$OFF, giPresets
    iPres3 tab_i 5+$OFF, giPresets
    iF$CHAN = iPres0^iC2 * iPres3^iC5
  endif
  chnset iF$CHAN, "$CHAN"
else
  iF$CHAN chnget "$CHAN"
endif
#

instr 1

iBase = p4
iVel = p5

; Learn boundary frequencies
iLearn_L chnget "learn_l"
if iLearn_L==1 then
  tabw_i iBase, 7, giFixed
endif
iLearn_M chnget "learn_m"
if iLearn_M==1 then
  tabw_i iBase, 8, giFixed
endif
iLearn_H chnget "learn_h"
if iLearn_H==1 then
  tabw_i iBase, 9, giFixed
endif

;iFl = 20
;iFm = 440
;iFh = 20000
iFl tab_i 7, giFixed
iFm tab_i 8, giFixed
iFh tab_i 9, giFixed

; non-interpolated parameters:
iVexp chnget "vexp"
iPbrange chnget "pbend"
imode chnget "wform"
iOrder chnget "order"

; Modify velocity curve (gamma correction):
iVel = iVel^iVexp

; If "play" Interpolate from registers:
iPlay chnget "play"
$SETLOG(fmin'0)
$SETLIN(frange'16)
$SETLIN(vib'32)
$SETLOG(fvib'48)
$SETLIN(trem'64)
$SETLOG(ftrem'80)
$SETLIN(width'96)
$SETLOG(volume'112)
$SETLIN(fatt'128)
$SETLIN(fdec'144)
$SETLIN(att'160)
$SETLIN(dec'176)
$SETLIN(sus'192)
$SETLIN(rel'208)

; Appply pitch bend:
kpb pchbend 0,iPbrange
kBase = iBase * 1.059463^kpb

; Calculate vibrato and tremolo:
kVibo lfo iFvib, iFfvib
kTremo lfo iFtrem, iFftrem

; Generate base waveform:
asig vco2 iFvolume*(1.122018^kTremo), kBase*(1.059463^kVibo), imode, iFwidth

; Apply filter (with envelope):
iFfmax = iFfmin * (2^iFfrange)
kFfreq transeg iFfmin, iFfatt, 0, iFfmax, iFfdec, -2, iFfmin
if iOrder>0 then
  asig1 tonex asig, kFfreq, iOrder
else
  asig1=asig
endif

; Calculate envelope:
kEnv transegr 0, iFatt, 0, 1, 10-iFatt, (iFatt-10)/iFdec, iFsus, iFrel, -10,0

; If used with reverb:
gaDry = gaDry + asig1*kEnv

; If used without reverb:
; outs asig1*kEnv, asig1*kEnv

endin


; This instrument adds the reverb and controls the VU meter:

instr 10

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"

aLrev, aRrev freeverb gaDry, gaDry, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaDry + kWet*aLrev
aRtot = kDry*gaDry + kWet*aLrev

outs aLtot, aRtot

ktrig metro 25
kVU max_k gaDry, ktrig, 1
if (ktrig==1) then
  chnset kVU, "vumeter"
  if (kVU>1) then
    chnset "trackerColour(255,0,0)", "vumeter_id"
  else
    chnset "trackerColour(0,255,0)", "vumeter_id"
  endif
endif

clear gaDry

endin


; This instrument controls the registers:

#define RCONTROL(CHAN'OFF) #
kF$CHAN chnget "$CHAN"

if ( (changed(kLearn_Lf)==1) && (kLearn_Lf==1) ) then
  kPres tab $OFF, giPresets
  chnset kPres, "$CHAN"
endif
if ( (changed(kLearn_Mf)==1) && (kLearn_Mf==1) ) then
  kPres tab 1+$OFF, giPresets
  chnset kPres, "$CHAN"
endif
if ( (changed(kLearn_Hf)==1) && (kLearn_Hf==1) ) then
  kPres tab 2+$OFF, giPresets
  chnset kPres, "$CHAN"
endif
if ( (changed(kLearn_Lp)==1) && (kLearn_Lp==1) ) then
  kPres tab 3+$OFF, giPresets
  chnset kPres, "$CHAN"
endif
if ( (changed(kLearn_Mp)==1) && (kLearn_Mp==1) ) then
  kPres tab 4+$OFF, giPresets
  chnset kPres, "$CHAN"
endif
if ( (changed(kLearn_Hp)==1) && (kLearn_Hp==1) ) then
  kPres tab 5+$OFF, giPresets
  chnset kPres, "$CHAN"
endif

if (changed(kF$CHAN)==1) then
  if (kLearn_All==1) then
    tabw kF$CHAN, 0+$OFF, giPresets
    tabw kF$CHAN, 1+$OFF, giPresets
    tabw kF$CHAN, 2+$OFF, giPresets
    tabw kF$CHAN, 3+$OFF, giPresets
    tabw kF$CHAN, 4+$OFF, giPresets
    tabw kF$CHAN, 5+$OFF, giPresets
  endif
  if (kLearn_L==1) then
    tabw kF$CHAN, 0+$OFF, giPresets
    tabw kF$CHAN, 3+$OFF, giPresets
  endif
  if (kLearn_M==1) then
    tabw kF$CHAN, 1+$OFF, giPresets
    tabw kF$CHAN, 4+$OFF, giPresets
  endif
  if (kLearn_H==1) then
    tabw kF$CHAN, 2+$OFF, giPresets
    tabw kF$CHAN, 5+$OFF, giPresets
  endif
  if (kLearn_f==1) then
    tabw kF$CHAN, 0+$OFF, giPresets
    tabw kF$CHAN, 1+$OFF, giPresets
    tabw kF$CHAN, 2+$OFF, giPresets
  endif
  if (kLearn_p==1) then
    tabw kF$CHAN, 3+$OFF, giPresets
    tabw kF$CHAN, 4+$OFF, giPresets
    tabw kF$CHAN, 5+$OFF, giPresets
  endif
  if (kLearn_Lf==1) then
    tabw kF$CHAN, 0+$OFF, giPresets
  endif
  if (kLearn_Mf==1) then
    tabw kF$CHAN, 1+$OFF, giPresets
  endif
  if (kLearn_Hf==1) then
    tabw kF$CHAN, 2+$OFF, giPresets
  endif
  if (kLearn_Lp==1) then
    tabw kF$CHAN, 3+$OFF, giPresets
  endif
  if (kLearn_Mp==1) then
    tabw kF$CHAN, 4+$OFF, giPresets
  endif
  if (kLearn_Hp==1) then
    tabw kF$CHAN, 5+$OFF, giPresets
  endif
;  ftprint giPresets, -1, 0, 224, 1, 16
endif
#

instr 20

kLearn_All chnget "learn_all"
kLearn_L chnget "learn_l"
kLearn_M chnget "learn_m"
kLearn_H chnget "learn_h"
kLearn_Lf chnget "learn_lf"
kLearn_f chnget "learn_f"
kLearn_Mf chnget "learn_mf"
kLearn_Hf chnget "learn_hf"
kLearn_p chnget "learn_p"
kLearn_Lp chnget "learn_lp"
kLearn_Mp chnget "learn_mp"
kLearn_Hp chnget "learn_hp"
kPlay chnget "play"
;printk 1,kLearn_All

$RCONTROL(fmin'0)
$RCONTROL(frange'16)
$RCONTROL(vib'32)
$RCONTROL(fvib'48)
$RCONTROL(trem'64)
$RCONTROL(ftrem'80)
$RCONTROL(width'96)
$RCONTROL(volume'112)
$RCONTROL(fatt'128)
$RCONTROL(fdec'144)
$RCONTROL(att'160)
$RCONTROL(dec'176)
$RCONTROL(sus'192)
$RCONTROL(rel'208)

SLoad chnget "load"
if (changed(SLoad)==1) then
; This is a crude check whether the output of 'load' makes sense:
  kPosmset strrindexk SLoad, ".mset"
  kLenload strlenk SLoad
; with Csound 6.17: if (filevalid:k(SLoad)==1) then
  if ((kPosmset>0)&&(kLenload-kPosmset==5)) then
    ftloadk SLoad, k(1), 1, giPresets, giFixed
; ftprint giFixed, -1
    kX tab 0, giFixed
    chnset kX, "vexp"
    kX tab 1, giFixed
    chnset kX, "pbend"
    kX tab 2, giFixed
    chnset kX, "wform"
    kX tab 3, giFixed
    chnset kX, "order"
    kX tab 4, giFixed
    chnset kX, "room"
    kX tab 5, giFixed
    chnset kX, "damp"
    kX tab 6, giFixed
    chnset kX, "wet"
    chnset k(1), "play"
    chnset k(0), "learn_all"
    chnset k(0), "learn_l"
    chnset k(0), "learn_m"
    chnset k(0), "learn_h"
    chnset k(0), "learn_lf"
    chnset k(0), "learn_mf"
    chnset k(0), "learn_hf"
    chnset k(0), "learn_lp"
    chnset k(0), "learn_mp"
    chnset k(0), "learn_hp"
;  else
;    printks "Preset file %s not found!\n", 0, SLoad
  endif
endif

SSave chnget, "save"
if (changed(SSave)==1) then
  kX chnget "vexp"
  tabw kX, 0, giFixed
  kX chnget "pbend"
  tabw kX, 1, giFixed
  kX chnget "wform"
  tabw kX, 2, giFixed
  kX chnget "order"
  tabw kX, 3, giFixed
  kX chnget "room"
  tabw kX, 4, giFixed
  kX chnget "damp"
  tabw kX, 5, giFixed
  kX chnget "wet"
  tabw kX, 6, giFixed
  ftsavek SSave, k(1), 1, giPresets, giFixed
endif

endin


</CsInstruments>
<CsScore>
;causes Csound to run for about 7000 years...
f0 z
i10 0 z
i20 0 z
</CsScore>
</CsoundSynthesizer>
