<Cabbage>
;form caption("Anykey") size(404, 90), guiMode("queue") pluginId("anyk")
form caption("Anykey (debug version)") size(440, 500), guiMode("queue") pluginId("anyk") ; debug version

nslider bounds(20, 20, 30, 22), channel("key0"), range(1, 127, 1, 1, 1)
nslider bounds(60, 20, 30, 22), channel("key1"), range(1, 127, 127, 1, 1)
label bounds(20, 50, 70, 13) text("MIDI range")
button bounds(110, 20, 20, 20) channel("ccact"), text("C"), colour:1("red")
nslider bounds(140, 20, 30, 22), channel("cc"), range(1, 127, 64, 1, 1)
label bounds(110, 50, 60, 13) text("MIDI CC")

nslider bounds(190, 20, 40, 22) channel("pos"), range(0, 2047, 0, 1, 1)
nslider bounds(190, 50, 40, 22) channel("pos0"), range(0, 2047, 0, 1, 1), automatable(0)
button bounds(242, 21, 20, 20), channel("restart"), latched(0), text("R"), colour:1("red")

combobox bounds(282, 21, 100, 20), channelType("string"), channel("keyfile"), populate("*.keys","Anykey/") ; keys on subdirectory
; combobox bounds(282, 21, 100, 20), channelType("string"), channel("keyfile"), populate("*.keys") ; keys on csd directory

csoundoutput bounds(20, 80, 400, 400) channel("csout") ; debug version

</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d -Q0
</CsOptions>
<CsInstruments>
; Initialize the global variables.
ksmps = 32
nchnls = 2
0dbfs = 1

massign 0, 0


; This instrument just watches channel "keyfile" and starts instr 2:

instr 1

  Scsdpath chnget "CSD_PATH"
  prints "CSD_PATH: \"%s\"\n", Scsdpath

  Sfile chnget "keyfile"
  if changed(Sfile)>0 then
    turnoff2 2, 0, 0
    event "i", 2, 0, 1000000
  endif

endin


; This instrument does the real work:

instr 2

  knoteon init 0
  
  cabbageSetValue "pos", 0
  cabbageSetValue "pos0", 0
  
  Sfile chnget "keyfile"
  iflen strlen Sfile
  Sfext strsub Sfile, iflen-5
  prints "Filename: \"%s\"\n", Sfile
;  prints "Extension: \"%s\"\n", Sfext
  if strcmp(Sfext, ".keys")==0 then
    Sfile1 = Sfile
  else
    Sfile1 strcat Sfile, ".keys"
  endif
  Sfile2 sprintf "%s/Anykey/%s", chnget:S("CSD_PATH"), Sfile1
  prints "Full filename: \"%s\"\n", Sfile2
  idum ftgen 1, 0, 2048, -7, 0, 2048, 0
  ftload Sfile2, 1, 1
  ilength = ftlen(1)
  prints "Length: %i\n", ilength

  key0 chnget "key0"
  key1 chnget "key1"
  if changed(key0)>0 then
    if key0>key1 then
      key0=key1
      cabbageSetValue "key0", key1
    endif
  endif
   if changed(key1)>0 then
    if key0>key1 then
      key1=key0
      cabbageSetValue "key1", key0
    endif
  endif

  kccact chnget "ccact"
  kcc chnget "cc"

  kpos0 chnget "pos0"
  kpos chnget "pos"

  kRestart chnget "restart"

  if ((changed(kRestart)==1)&&(kRestart==1))||(changed(kpos0)==1) then
    kpos = kpos0
    cabbageSetValue "pos", kpos
  endif

  kstati, kchani, kdata1i, kdata2i midiin
  kstato = kstati
  kdata1o = kdata1i
  kdata2o = kdata2i

  if kccact==0 then
    if (kdata1i>=key0)&&(kdata1i<=key1) then
      if kstati==144 then
        kdata1o table kpos, 1, 0
        tablew kdata1o, kdata1i, 2
        kpos = kpos+1
        if kpos>=ilength then
          kpos = 0
        endif
        cabbageSetValue "pos", kpos
      elseif kstati==128 then
        kdata1o table kdata1i, 2
      endif      
    else
      if (kstati==144)||(kstati==128) then
        kstato = 0
      endif
    endif
  else
    if kstati==176 then
      if kdata1i==kcc then
        if (kdata2i==127)&&(knoteon==0) then
          kdata1o table kpos, 1, 0
          tablew kdata1o, 0, 2
          kpos = kpos+1
          if kpos>=ilength then
            kpos = 0
          endif
          cabbageSetValue "pos", kpos
          kstato = 144
          kdata2o = 64
          knoteon = 1
        elseif kdata2i==0 then
          kdata1o table 0, 2
          kstato = 128
          kdata2o = 0
          knoteon = 0
        else
          kstato = 0
        endif
      endif
    elseif (kstati==144)||(kstati==128) then
      kstato = 0
    endif
  endif

  if kstato!=0 then
    printks "%3i : %3i %3i %3i | %3i %3i %3i\n", 0, kpos, kstati, kdata1i, kdata2i, kstato, kdata1o, kdata2o
    midiout kstato, kchani, kdata1o, kdata2o
  endif

endin

</CsInstruments>
<CsScore>
;causes Csound to run for about 7000 years...
f0 z
;starts instrument 1 and runs it for a a Ms
i1 0 1000000
;empty notes table
;f 1 0 2048 -7 0 2048 0
;note-on table
f 2 0 128 -7 0 128 0
</CsScore>
</CsoundSynthesizer>
