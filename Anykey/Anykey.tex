 % !TeX spellcheck = en_GB
 \documentclass[12pt,a4paper]{article}

\usepackage{environ,amsmath}
\usepackage[
colorlinks=true,
urlcolor=blue,
linkcolor=green
]{hyperref}

\textwidth 160mm
\textheight 247mm
\hoffset -15mm
\voffset -20mm
\parindent=0pt
\parskip=6pt
\sloppy

\def\bb{\rule{24pt}{8.2pt}}
\def\bc{\rule{5.875pt}{7.333pt}}
\def\em{\bf}
\def\d{{\mathrm d}}
\def\eq#1{(\ref{#1})}
\def\vec#1{\mathbf{#1}}
\def\note#1{\bb~{\sf #1}}
\def\unit#1{\,{\rm #1}}

\NewEnviron{eqs}{%
	\begin{equation}
		\begin{split}
			\BODY
		\end{split}
\end{equation}}
\NewEnviron{eqm}{%
	\begin{multline}
		\BODY
\end{multline}}

\def\R#1{{\color{red} #1}}
\def\G#1{{\color{green} #1}}
\def\B#1{{\color{blue} #1}}
\def\Y#1{{\color[rgb]{0.8,0.5,0} #1}}

\def\d{{\rm d}}
\def\e{{\rm e}}
\def\i{{\rm i}}
\def\AArez{{\,\mbox{\AA}^{-1}}}
\def\Ar{{\AA$^{-1}$}}
\def\({\left(}
\def\){\right)}
\def\<{\langle}
\def\>{\rangle}
\def\s#1{\hspace{#1cm}}
\def\bb{\rule{24pt}{8.2pt}}
\def\note#1{\bb~{\sf #1}}
\def\eq#1{(\ref{#1})}
\def\Ref#1{ref.~\citenum{#1}}

\begin{document}

\begin{center}
	{\LARGE\bf Anykey}\\[5mm]
	{\bf Version of \today}\\[10mm]
	{\it Reiner Zorn}\\[10mm]
\end{center}

{\it Press any key to continue \dots}\\[10mm]
Anykey is the result of my inability to play multi-part pieces without errors. The basic idea is to replace the keys played by the ones with the correct MIDI note numbers. This is a similar concept as that of Stephen Malinowski´s `Tapper' (see section~\ref{tapper}). I first thought about realising this as a JS plugin in REAPER, but later decided to do this in csound to make the plugin available also in other DAWs.

\section{Using Anykey}
For keyboard playing, Anykey will replace the note number of a note-on MIDI command with the `correct' value from the keys file selected in the rightmost drop-down menu. It does so only for the note numbers in the specified range. So, other notes may still be played in the usual way. The conversion is quasi-instantaneous, so that there is no change in rhythm (no `quantisation'). Also, the velocity value is forwarded without change.

The keys file will be played from the beginning (index 0) with the current position displayed and selectable in the fifth field in the upper row. Instead of starting from 0, a cue point may be set in the field below. The `R' button besides resets the file to the beginning or the cue point.

Every note played has its individual note-off too. Therefore, it is possible to play notes in a sequence overlappingly. In principle, this can be used to generate arpeggios, but with the restriction that the sequence of notes cannot be influenced by the player.

The `C' button allows to control a voice by MIDI CC commands in addition to allow the use of pedals or similar. The current version is designed for using a sustain pedal (CC64 by default). It responds only to the CC value 127 resulting in a note-on with velocity 64, and 0 triggering a note-off. (The reason for this is that the pedal I use also generates spurious CC64s with values in between which are better ignored.) A future extension would be to implement MIDI devices which also produce a value which can be meaningfully used as velocity.

In the effects chain of the DAW, Anykey has to be placed before the instrument to be played. Note that, depending on the DAW it may be necessary to put additional MIDI note or CC filters in the chain to suppress direct action of the keys or the default action of the sustain pedal. A working REAPER project is included.

\section{Generating a keys file}
Anykey requires a file with the MIDI key numbers of the part of the piece to be played. The file must have the extension {\tt .keys} and be stored in the subdirectory {\tt Anykey} under the directory of the file {\tt Anykey.csd}. It is a standard csound table with one entry per note, the MIDI key number. No length or velocity information is included.

In principle, such a file can be constructed in a text editor from the notes in a printed score. But it is easier to extract the information from a MIDI file.
To extract a single part as a file which can be read by Anykey, a quick-and-dirty Python script, {\tt midi2keys.py}, is provided. It requires a MIDI file with separate tracks each containing only one part. To my experience, most downloadable MIDI files of typical multi-part pieces (Bach etc.) are sorted in that way. If this is not the case, one would have to use a MIDI editor to split the file into separate ones.

\section{Comparison to Tapper}\label{tapper}
Some readers will immediately recognise the similarity of this plug-in to that of Stephen Malinowski´s~\cite{wiki:Malinowski} `Tapper'. The basic idea~\cite{Tapper} to relieve the player from the burden of memorising notes or sight-reading is the same for both. The most important difference is that Anykey is voice- or note-based, while Tapper is score-based. This means that in Tapper a complete score can be played with one finger while in Anykey the player needs at least as many fingers as there are parts. Of course, this is more complicated for the player, but it allows more freedom in the performance:

\begin{itemize}
	\item Each part can be played with its own rhythm. E.g., one part may be played with a `swing' while another strict to the beat.
	\item Each part can be played with its own velocities.
	\item Ornaments my be applied. In the current version, the number of notes is fixed but the speed distribution of the ornament could be varied.
	\item Anykey-controlled parts can be combined with free parts for improvisations over a fixed `ostinato'.
\end{itemize}

Of course, this comes at a price, and here are the advantages of Tapper:

\begin{itemize}
	\item First of all, it is easier to play because all voices are controlled with one finger.
	\item Especially, chords will be played with one stroke and exact timing. Using Anykey it is possible to generate chords within a part by not releasing notes. But in a strict sense these are arpeggios and not synchronous note-ons.
\end{itemize}

In summary, Anykey has its advantages for pieces like the two-part inventions or chorale preludes of Bach while Tapper shows its strengths in works of mor orchestral character.

Originally, one of the motivations to write Anykey was that Tapper is a stand-alone program. But because the most recent version of Tapper is MIDI-based it is comparatively easy to use in a DAW by linking it through virtual MIDI devices, e.g., using loopMIDI~\cite{loopMIDI}. So now Tapper can also be used from within a DAW in a comparatively easy way.

\section{Example file}
As an example a playable REAPER project of Johann Sebastian Bach's chorale prelude `Ich ruf' zu Dir´ (BWV 639) is added. The second voice is played with the notes C2--G2 and the third (pedal) voice with the sustain pedal. The sound will be generated by my instrument Etude \#1 and Softube's Saturation Knob. Of course, you can use any VST instrument instead. A midi event filter (REAPER stock plugin) is added to avoid the default action of the sustain pedal.

\section{Known bugs and problems}
Sometimes, the first keys file in the combobox is not loaded when REAPER is started. (This may happen for other DAWs too.) In that case, load one of the other files and then the first one again. The supposed behaviour in a DAW is that all fields are saved when the project file is saved. Sometimes, this seems not to work correctly for the position in the score and the `cue point'. If that problem persists, load one of the other keys files and then the first one again. By that, both position and cue point will be reset to zero.

\section{Additional features to be implemented}
A better implementation of foot-operated MIDI controllers would be nice. The use of the sustain pedal for that purpose is clearly not optimal. A MIDI controller with velocity signal would be better, and it would have to be included in the code that its MIDI CC value is converted to the note-on velocity.

A more complicated feature would be bring Anykey a bit closer to Tapper with the option of controlling multiple parts with one key. That feature would be interesting, e.g., to free the player of an organ work from using the pedal at all and have the computer add the correct bass notes according to one of the manual voices. The problem here is that the keys file would have to include information on the timing, so that the parts are played synchronously. 

\bibliographystyle{RZ}
\bibliography{csound}

\end{document}
