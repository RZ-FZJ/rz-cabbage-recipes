; Etude #2 version 2

<Cabbage> bounds(0, 0, 0, 0)

form caption("Etude #2") size(705, 420), colour(58, 110, 182), pluginId("etu2")

; first row: input-related controls
rslider bounds(94, 14, 70, 70), channel("vexp"), range(0, 2, 1, 0.5, 0.01), text("VelExp")

vslider bounds(254, 10, 30, 70), channel("vib"), range(0, 1, 0, 0.5, 0.01)
button bounds(224, 14, 22, 22), channel("mvib"), text("M"), colour:1("red")
rslider bounds(218, 42, 34, 34), channel("fvib"), range(0.1, 20, 5, 1, 0.01)
label bounds(212, 76, 66, 12), text("Vibrato"), colour(0,0,0,0)

button bounds(290, 48, 22, 22), channel("mfcouple"), text("="), colour:1("red")

vslider bounds(356, 10, 30, 70), channel("trem"), range(0, 5, 0, 1, 0.1)
button bounds(326, 14, 22, 22), channel("mtrem"), text("M"), colour:1("red")
rslider bounds(320, 42, 34, 34), channel("ftrem"), range(0.1, 20, 5, 1, 0.01)
label bounds(320, 76, 66, 12), text("Tremolo"), colour(0,0,0,0)

rslider bounds(424, 14, 70, 70), channel("pbend"), range(0, 12, 2, 1, 0.01), text("PBendRng")

combobox bounds(504, 14, 100, 25), channelType("string"), channel("comboChannel"), populate("Etude#2.snaps")
filebutton bounds(514, 50, 25, 25), channel("save"), text("+"), mode("named snapshot"), populate("Etude#2.snaps")
filebutton bounds(544, 50, 25, 25), channel("remove"), text("-"), mode("remove preset"), populate("Etude#2.snaps")

; second row: instrument-related controls:
; rslider bounds(12, 94, 70, 70), channel("npart"), range(1, 32, 10, 1, 1), text("Partials")
rslider bounds(12,94,70,70), channel("dim"), range(2,8,8,1,1), text("Dimension")
combobox bounds(82,94,50,25), channel("seq1"), items("all",">0","odd"), value(1)
combobox bounds(82,129,50,25), channel("seq2"), items("all",">0","odd"), value(1)
; rslider bounds(132, 94, 70, 70), channel("xarat"), range(-0.99,0.99,0, 1, 0.01), text("Aspect Ratio")
encoder bounds(140, 119, 32, 32), channel("xarat"), min(-0.99), max(0.99), value(0), increment(0.01);, text("Aspect Ratio")
button bounds(180, 124, 22, 22), channel("carat"), text("C"), colour:1("red")
nslider bounds(142, 94, 60, 20), channel("arat"), range(0.001, 999, 1, 1, 0.001)
label bounds(140, 148, 63, 20), text("Aspect Ratio"), fontSize(11)
rslider bounds(205, 94, 70, 70), channel("fexp"), range(0, 3, 1, 1, 0.01), text("Exponent")
button bounds(272, 94, 22, 22), channel("xmode"), text("*"), colour:1("red")
rslider bounds(292, 94, 70, 70), channel("pdecay"), range(0, 1, 0, 0.3, 0.001), text("P.Damp.")

rslider bounds(362, 94, 70, 70), channel("att"), range(0.01, 1, 0.02, 0.5, 0.001), text("Attack")
rslider bounds(432, 94, 70, 70), channel("dec"), range(0, 1, 0.5, 1, .01), text("Decay")
vslider bounds(497, 94, 30, 70), channel("xdec"), range(0, 1, 0, 1, 0.01)
rslider bounds(522, 94, 70, 70), channel("sus"), range(0, 1, 0.1, 0.5, 0.01), text("Sustain")
rslider bounds(590, 94, 70, 70), channel("rel"), range(0, 5, 0.1, 1, 0.01), text("Release")
vslider bounds(655, 94, 30, 70), channel("relv"), range(0, 1, 1, 1, 0.01)

; third row: 'filter', reverb

rslider bounds(342, 174, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(412, 174, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(482, 174, 70, 70), channel("wet"), range(0, 1, 0.5, 1, .01), text("Dry/Wet")
rslider bounds(132, 174, 70, 70), channel("xlow"), range(0, 48, 0, 1, 0.1), text("Low")
rslider bounds(202, 174, 70, 70), channel("fcent"), range(10, 20000, 1000, 0.5, 1), text("Centre")
rslider bounds(272, 174, 70, 70), channel("xhigh"), range(0, 48, 0, 1, 0.1), text("High")


; soft keyboard:
keyboard bounds(14, 280, 676, 95), value(36)

</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
; Initialize the global variables. 
ksmps = 4
nchnls = 2
0dbfs = 1

gaDry init 0

;instrument will be triggered by keyboard widget
instr 1

ifrqs ftgentmp 0, 0, 64, 7, 0, 64, 0
iamps ftgentmp 0, 0, 64, 7, 0, 64, 0
idamp ftgentmp 0, 0, 64, 7, 0, 64, 0

iBase = p4
iAmp = p5
iVexp chnget "vexp"
iPbrange chnget "pbend"
kVib chnget "vib"
kFvib chnget "fvib"
kTrem chnget "trem"
kFtrem chnget "ftrem"
iDim chnget "dim"
iSeq1 chnget "seq1"
iSeq2 chnget "seq2"
iFexp chnget "fexp"
iXmode chnget "xmode"
iXarat chnget "xarat"
iArat chnget "arat"
ifdamp chnget "pdecay"
iAtt chnget "att"
iDec chnget "dec"
iXdec chnget "xdec"
iSus chnget "sus"
iRel chnget "rel"
iRelv chnget "relv"
iXlow chnget "xlow"
iXhigh chnget "xhigh"
iFcent chnget "fcent"

; print iBase

; During the release phase get release velocity:
kdata2 init 0
krel release
if (krel==1)&&(kdata2==0) then
  kstatus, kchan, kdata1, kdata2 midiin
  kEnvexp = 1/(1-(1-iRelv)*(kdata2-1)/126)
endif

; Modify velocity curve (gamma correction):
iAmp = iAmp^iVexp

; Appply pitch bend:
kpb pchbend 0,iPbrange
kBase = iBase * 1.059463^kpb

; Calculate the partials:
iIndex1 = 0
iTindex=0
isumamp = 0
if (iSeq1>1)&&(iSeq2>1) then
  iRebase=sqrt(1+iArat^2)
else
  iRebase min 1, iArat
endif
loop1:
  iIndx1 = ( iSeq1>2 ? 2*iIndex1 : iIndex1 )
  iIndx1 = ( iSeq1>1 ? iIndx1+1 : iIndx1 )
  iIndex2 = 0
  loop2:
    iIndx2 = ( iSeq2>2 ? 2*iIndex2 : iIndex2 )
    iIndx2 = ( iSeq2>1 ? iIndx2+1 : iIndx2 )
    ifreq = sqrt ( iIndx1^2 + (iArat*iIndx2)^2 ) / iRebase
    ipfreq = ifreq*iBase
    if (ifreq>0)&&(ipfreq<=(sr/2)) then
      if (iXmode==0) then
        ipamp = (ifreq^(-iFexp)) * ((1+(iFcent/ipfreq))^(-iXlow/12)) * ((1+(ipfreq/iFcent))^(-iXhigh/12))
      else
        ipamp = ((max(1, iIndx1)*max(1, iIndx2))^(-iFexp)) * ((1+(iFcent/ipfreq))^(-iXlow/12)) * ((1+(ipfreq/iFcent))^(-iXhigh/12))
      endif
      isumamp = isumamp + ipamp^2
      tablew ifreq, iTindex, ifrqs
      tablew ipamp, iTindex, iamps
      idmp = exp ( - (ksmps/sr) * ifdamp * (ipfreq-iBase) )
      tablew idmp, iTindex, idamp
;      print iIndx1, iIndx2, ifreq, ipamp, idmp
      iTindex = iTindex + 1
    endif
  loop_lt iIndex2,1,iDim,loop2
loop_lt iIndex1,1,iDim,loop1
iNpart = iTindex

; Dampen higher partials over time:
kindex = 0
loopk:
  kamp1 table kindex, iamps
  kdmp1 table kindex, idamp
  tablew kamp1*kdmp1, kindex, iamps
loop_lt kindex,1,iNpart,loopk

; Calculate vibrato and tremolo:
kVibo lfo kVib, kFvib
kTremo lfo kTrem, kFtrem

; Generate sound:
asig adsynt2 0.5*iAmp/sqrt(isumamp)*(1.122018^kTremo), kBase*(1.059463^kVibo), -1, ifrqs, iamps, iNpart

; Calculate envelope:
kEnvraw transegr 0, iAtt, 0, 1, 10-iAtt, (iAtt-10)/(iDec/((iBase/440)^iXdec)), iSus, iRel, -10,0

; Modify envelope during release phase:
kStartRel init 0
if krel==0 then
  kEnv = kEnvraw
else
  if kEnvraw==0 then
    kEnv = 0
  else
    if kStartRel==0 then
      kStartRel = kEnvraw
    endif
    kEnv = (kEnvraw/kStartRel)^kEnvexp*kStartRel
  endif
endif

; If used with reverb:
gaDry = gaDry + asig*kEnv

; If used without reverb:
; outs gaDry, gaDry

endin


; This instrument sets the sliders from MIDI controls and controls the coupling of sliders:

instr 2

kMvib chnget "mvib"
kMtrem chnget "mtrem"
kFcouple chnget "mfcouple"
kFvib chnget "fvib"
kFtrem chnget "ftrem"
kXarat chnget "xarat"
kCCarat chnget "carat"
kArat chnget "arat"

if (kMvib!=0) then
  kVibm ctrl7 1, 1, 0, 1
  chnset kVibm, "vib"
endif

if (kMtrem!=0) then
  kTremm ctrl7 1, 1, 0, 5
  chnset kTremm, "trem"
endif

if (kFcouple!=0) then
  kCfvib changed kFvib
  if (kCfvib==1) then
    chnset kFvib, "ftrem"
  endif
  kCftrem changed kFtrem
  if (kCftrem==1) then
    chnset kFtrem, "fvib"
  endif
endif

kCarat changed kArat
if (kCarat==1) then
  chnset (kArat-1)/(kArat+1), "xarat"
endif

if (kCCarat!=0) then
  kXarat ctrl7 1, 10, -0.99, 0.99 
  chnset kXarat, "xarat"
endif

kCxarat changed kXarat
if (kCxarat==1) then
  chnset (1+kXarat)/(1-kXarat), "arat"
endif

endin


; This instrument adds the reverb:

instr 10

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"

aLrev, aRrev freeverb gaDry, gaDry, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaDry + kWet*aLrev
aRtot = kDry*gaDry + kWet*aLrev

outs aLtot, aRtot

clear gaDry

endin


</CsInstruments>
<CsScore>
i2 0 z
i10 0 z
</CsScore>
</CsoundSynthesizer>
