import os
from mido import MidiFile

print("\n*** Extract keys for Anykey from MIDI file ***\n")

midifn=input("MIDI file to extract from? ")
mid=MidiFile(midifn,clip=True)

tnlist=[]
for itrack,track in enumerate(mid.tracks):
  # print("++++++++++++++++++")
  # print(track)
  noten=0
  notes=[]
  for msg in track:
    if (msg.type=='note_on')and(msg.velocity>0):
      noten=noten+1
      notes.append(msg.note)
  print("Track %i: %i notes, starting with:" % (itrack,noten))
  print(notes[0:10])
  tnlist.append(notes)

strack=input("Covert which track? ")
itr=int(strack)
tfname=input("Anykey file name? ")

header="""======= TABLE 1 size: %i values ======
flen: %i
lenmask: 0
lobits: 0
lomask: 0
lodiv: 0.000000
cvtbas: 0.000000
cpscvt: 0.000000
loopmode1: 0
loopmode2: 0
begin1: 0
end1: 0
begin2: 0
end2: 0
soundend: 0
flenfrms: 0
nchnls: 0
fno: 1
gen01args.gen01: 0.000000
gen01args.ifilno: 0.000000
gen01args.iskptim: 0.000000
gen01args.iformat: 0.000000
gen01args.channel: 0.000000
gen01args.sample_rate: 0.000000
---------END OF HEADER--------------
"""
footer="""0.000000
---------END OF TABLE---------------
"""

tf=open(tfname,'w')
nnum=len(tnlist[itr])
tf.write(header % (nnum,nnum))
for note in tnlist[itr]:
  tf.write("%0.6f\n" % note)
tf.write(footer)
tf.close()
