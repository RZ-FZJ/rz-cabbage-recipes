 % !TeX spellcheck = en_GB
 \documentclass[12pt,a4paper]{article}

\usepackage{environ,amsmath}
\usepackage[
colorlinks=true,
urlcolor=blue,
linkcolor=green
]{hyperref}

\textwidth 160mm
\textheight 247mm
\hoffset -15mm
\voffset -20mm
\parindent=0pt
\parskip=6pt
\sloppy

\def\bb{\rule{24pt}{8.2pt}}
\def\bc{\rule{5.875pt}{7.333pt}}
\def\em{\bf}
\def\d{{\mathrm d}}
\def\eq#1{(\ref{#1})}
\def\vec#1{\mathbf{#1}}
\def\note#1{\bb~{\sf #1}}
\def\unit#1{\,{\rm #1}}

\NewEnviron{eqs}{%
	\begin{equation}
		\begin{split}
			\BODY
		\end{split}
\end{equation}}
\NewEnviron{eqm}{%
	\begin{multline}
		\BODY
\end{multline}}

\def\R#1{{\color{red} #1}}
\def\G#1{{\color{green} #1}}
\def\B#1{{\color{blue} #1}}
\def\Y#1{{\color[rgb]{0.8,0.5,0} #1}}

\def\d{{\rm d}}
\def\e{{\rm e}}
\def\i{{\rm i}}
\def\AArez{{\,\mbox{\AA}^{-1}}}
\def\Ar{{\AA$^{-1}$}}
\def\({\left(}
\def\){\right)}
\def\<{\langle}
\def\>{\rangle}
\def\s#1{\hspace{#1cm}}
\def\bb{\rule{24pt}{8.2pt}}
\def\note#1{\bb~{\sf #1}}
\def\eq#1{(\ref{#1})}
\def\Ref#1{ref.~\citenum{#1}}

\begin{document}

\begin{center}
	{\LARGE\bf Etude \# 3}\\[5mm]
	{\bf Version of \today}\\[10mm]
	{\it Reiner Zorn}\\[20mm]
\end{center}

Etude~\#~3 is a more experimental instrument with the aim of interpolating between the csound opcodes {\tt buzz} and {\tt dust}. The basic objective is to generate a sequence of pulses with varying randomness. In the language of stochastic processes, the `waiting time distribution', i.e., the distribution of times between two consecutive pulses, of {\tt dust} is $ \phi(t) = f \exp (- f \cdot t) $. This time sequence is called `Poisson process'. For {\tt buzz}, the pulses are equally spaced by the time $1/f$ which correspond to a delta distribution, $ \phi(t) = \delta(t-1/f)$.

\section{Waiting time distribution}
A distribution which interpolates between both processes is the gamma distribution,
\begin{equation}\label{key}
	\phi(t) = { \lambda^\alpha \over \Gamma(\alpha) } t^{\alpha-1} \exp (-\lambda t) \,. 
\end{equation}
It is immediately clear that for $\alpha=1$ this reduces to the exponential distribution. The mean time interval is $ \<t\> = \alpha/\lambda$, leading to the correspondence $ \lambda = \alpha f $. The width of the time distribution (more pecisely, the square root of the variance) is $ \Delta t = \sqrt\alpha/\lambda $ and the relative width $ w = \Delta t / \<t\> = 1 / \sqrt\alpha $. Therefore, $ \alpha \to \infty $ corresponds to equidistant pulses. Notably, the gamma distribution is also defined for $0<\alpha<1$; in that case the width is larger than that of a Poisson process.

Although the parameters of the gamma distribution are simple to calculate from given $f$ and $w$, it is more difficult to generate random values according to that distribution. To this end, Etude~\#~3 contains a user-defined opcode {\tt gamrand} based on the algorithm of Marsaglia and Tsang~\cite{Ma00}.~

An algorithmically simpler way to generate a transition between an exponential distribution and undistributed values is to transform exponentially distributed random values $\tau$ with average 1 by a power law and rescale them by a factor $c$: $ \tau \to c \tau^\gamma $. The distribution of values generated in this way is
\begin{equation}\label{key}
	\phi(t) = { 1 \over \gamma c^{1/\gamma} } t^{1/\gamma-1} \exp \(-(t/c)^{1/\gamma}\) \,. 
\end{equation}
On the first glance, this looks similar to the gamma distribution but the characteristic difference is that there is an inner exponent $1/\gamma$ in the exponential term\footnote{I discovered later that this is known as the Weybull distribution which is already implemented in csound.}. Because for most uses $\gamma<1$, this has a strong influence on the probability of large time intervals; they are much more suppressed here than they are for the gamma distribution. Again it is clear that for $\gamma=1$ this reduces to the exponential distribution. On the other hand for $\gamma=0$ it is obvious from the generation of random variables that a constant value $c$ is obtained.

While the generation of random values is easier here, the calculation of average time and width is more complicated:
\begin{align}
	\<t\> &= c \, \Gamma(1+\gamma) \\
	w &= \sqrt{ \Gamma(1+2\gamma)/\Gamma^2(1+\gamma) - 1 }
\end{align}
For calculating the parameters $\gamma$ and $c$ of the distribution, these equations have to be solved. Since this calculation is virtually impossible in csound there are two tables (1001 and 1002) in the score with tabulated values of the solutions. From these the parameters are calculated from $f$ and $w$ by interpolation.

\section{Anti-aliasing}
A challenging part of making this instrument was the handling of aliasing. While for mostly random sequences of pulses ({\tt dust}) aliasing is not noticeable because the spectrum is anyway filled with random components, as soon as one gets close to the {\tt buzz} situation, aliasing becomes audible when playing a sequence of pulses. For that reason, {\tt buzz} constructs the signal not of individual pulses but by additive synthesis from a finite sequence of harmonics. That is not possible here in the intermediate situation when there is still some randomness in the time intervals between pulses.

The question how to obtain an aliasing-free signal from a given ideal signal seems to have no general answer. But in the case of sequences of `ideal' (i.e.\ delta function shaped) pulses there is one. Ideally, each pulse should be replaced by the inverse Fourier transform of a box window (in frequency) whose width is half the sampling frequency $f_{\rm s}$,
\begin{equation}\label{pulse}
	{ \sin \( \pi f_{\rm s} t \) \over \pi t} \,.
\end{equation}

The disadvantage of this function is that it decays only with $t^{-1}$ in time. So, a large sample would have to be played for each instance of a pulse which would probably lead to a prohibitive CPU load at high frequencies. The solution is to multiply the pulse function \eq{pulse} by a window in time. Admittedly, this leads to a softening of the cut-off in frequency potentially allowing aliasing to reappear. But from signal processing theory several window functions are known to perform well in that respect. Here, the Kaiser window~\cite{wiki:KW} with $a=2$ was chosen for that purpose:
\begin{equation}\label{window}
	{ I_0 \( a \pi \sqrt{1 - t^2 / T^2} \) \over I_0 (a\pi)} \mbox{ for } |t|\le T
\end{equation}
Here, $I_0$ denotes the zeroth-order Bessel function of the first kind. The cut-off time $T$ was chosen as $6/f_{\rm s}$ so that there are six oscillations of the sine in equation~\eq{pulse} represented. The product of \eq{pulse} and \eq{window} was calculated at 128 points and stored in table 1010 in the score. It turns out that generating the audio signal from this table is sufficient to completely suppress aliasing audibly and monitored by a spectrum analyser. On the other hand, the limit of the time range ensures that only six lookups from the table per pulse are necessary.

\section{Controls}
The main controls of Etude~\#~3 are the knobs {\sf Frequency} and {\sf Width} defining the nominal frequency, $ f = 1 / \< t \> $ in terms of the average distance between two pulses, and $w$ the relative width of the distribution of times. The small knobs allow a simple form of modulation. The upper one defines a multiplicative offset and the lower a time within which the value exponentially decays to the value given by the main knob. The {\sf F} button couples the frequency to the MIDI pitch allowing the instrument to be `played'. In that setting, {\sf Frequency} represent the frequency assigned to MIDI note 69. By default, this is 440~Hz.

For high width $w$ random numbers may produce pulse distance times which effectively let the instrument get stuck. For that reason, with the slider {\sf Lim}, the maximum time between pulses can be limited. The control is immediately active and can be used to resolve a `stuck' situation. The button {\sf Gamma/Weybull} switches between the two distributions mentioned above. The basic output of the instrument is a sequence of pulses with height 1. This can be changed to $\pm 1$ by the {\sf Bipolar} button. (A random amplitude as in the original {\tt dust} opcode may be added as a further option.)

{\sf Amp.\ Rand.}\ obviously means amplitude randomisation. It is included to produce the original {\tt dust} signal which has randomised pulse amplitudes in addition to randomised times between pulses. This is done by setting the control to 1. Setting it to 0 generates the `normal' $ 1 / \pm1 $ amplitudes. In between, amplitudes are generated by $ r^c $ where $r$ is a random number in the range $0\dots1$ and $c$ the value of the {\sf Amp.\ Rand.}\ control.

{\sf Attack}, {\sf Decay}, {\sf Sustain}, and {\sf Release} have the usual meanings. Presets can be selected in the combobox. New ones can be defined by {\sf +} and existing deleted by {\sf --}.

The pulse sequence can be filtered by {\sf Low Pass} which is inactive if the knob is turned to the right end. Also here coupling to the MIDI note and a simple modulation are possible. If the {\sf Low Pass} frequency lies significantly below the pulse generation frequency, the effect results in a randomised sawtooth (square wave if {\sf Bipolar} is selected). The {\sf Resonance} knob allows to control the second parameter of the csound filter {\tt lowpass2}. This allows a second way to `play' the instrument, namely producing pulses at low frequency which are converted into pitched sounds through the resonance of the filter.

{\sf Room Size}, {\sf Damping}, {\sf Dry/Wet} are the controls for {\tt freeverb}. The {\sf Volume} knob has a large range of three decades because the instrument tends to produce largely different amplitudes which have to be compensated to avoid clipping. This can be checked on the VU meter. Besides the piano keyboard, a button {\sf P} is added to permanently play a note which is useful to study the effect of varying controls continuously.

\section{Known bugs and problems}
Some presets are intended to have a transposition by setting the frequency and/or low pass frequency to a multiple of 440. If at the same time the follow button is activated by the preset that sets the frequency and low pass frequency back to 440. As a result, the transposition does not work. When the same preset is loaded a second time it works. This behaviour is caused by the way the combobox-snaps mechanism works. To fix this would probably require the whole preset management to be `hand-coded'.

Because the pulse sequence is generated directly by the csound code and not by some opcodes the instrument has to run with {\tt ksmps = 1}. That means k-rate is as high as a-rate and the CPU load is 32$\times$ higher than for the default setting of csound. This becomes even more problematic when multiple instances are used in a DAW. So precautions like off-line rendering may have to be taken.

\bibliographystyle{RZ}
\bibliography{csound}

\end{document}
