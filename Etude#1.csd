<Cabbage> bounds(0, 0, 0, 0)

form caption("Etude #1") size(680, 420), colour(58, 110, 182), pluginId("et1b")

; first row: input-related controls

rslider bounds(54, 14, 70, 70), channel("vexp"), range(0, 2, 1, 0.5, 0.01), text("VelExp")

vslider bounds(224, 10, 30, 70), channel("vib"), range(0, 1, 0, 0.3, 0.001)
button bounds(194, 14, 22, 22), channel("mvib"), text("M"), colour:1("red")
rslider bounds(188, 42, 34, 34), channel("fvib"), range(0.1, 20, 5, 0.5, 0.01)
label bounds(182, 76, 66, 12), text("Vibrato"), colour(0,0,0,0)

button bounds(260, 48, 22, 22), channel("mfcouple"), text("="), colour:1("red")

vslider bounds(326, 10, 30, 70), channel("trem"), range(0, 5, 0, 0.5, 0.02)
button bounds(296, 14, 22, 22), channel("mtrem"), text("M"), colour:1("red")
rslider bounds(290, 42, 34, 34), channel("ftrem"), range(0.1, 20, 5, 0.5, 0.01)
label bounds(290, 76, 66, 12), text("Tremolo"), colour(0,0,0,0)

rslider bounds(394, 14, 70, 70), channel("pbend"), range(0, 12, 2, 1, 0.01), text("PBendRng")

combobox bounds(514, 14, 100, 25), channelType("string"), channel("comboChannel"), populate("Etude#1.snaps")
filebutton bounds(514, 50, 25, 25), channel("save"), text("+"), mode("named snapshot"), populate("Etude#1.snaps")
filebutton bounds(544, 50, 25, 25), channel("remove"), text("-"), mode("remove preset"), populate("Etude#1.snaps")

; second row: instrument-related controls:

rslider bounds(12, 94, 70, 70), channel("npart"), range(1, 32, 10, 1, 1), text("Partials")
rslider bounds(82, 94, 70, 70), channel("fexp"), range(0, 3, 1, 1, 0.01), text("Exponent")
rslider bounds(152, 94, 70, 70), channel("pdecay"), range(0, 1, 0, 0.3, 0.001), text("P.Damp.")
nslider fontSize(20), bounds(222, 94, 31, 60), channel("A"), range(1, 4, 1, 1, 1), text("A") 
nslider fontSize(20), bounds(252, 94, 31, 60), channel("B"), range(0, 2, 0, 1, 1), text("B")
nslider fontSize(20), bounds(282, 94, 31, 60), channel("C"), range(0, 4, 1, 1, 1), text("C")
rslider bounds(322, 94, 70, 70), channel("att"), range(0.01, 1, 0.02, 0.5, 0.001), text("Attack")
vslider bounds(385, 94, 30, 70), channel("xatt"), range(0, 1, 0, 1, 0.01)
rslider bounds(417, 94, 70, 70), channel("dec"), range(0, 1, 0.5, 1, .01), text("Decay")
vslider bounds(480, 94, 30, 70), channel("xdec"), range(0, 1, 0, 1, 0.01)
rslider bounds(507, 94, 70, 70), channel("sus"), range(0, 1, 0.1, 0.5, 0.01), text("Sustain")
rslider bounds(575, 94, 70, 70), channel("rel"), range(0, 5, 0.1, 1, 0.01), text("Release")
vslider bounds(640, 94, 30, 70), channel("relv"), range(0, 1, 1, 1, 0.01)

; third row: 'filter', reverb, total volume

rslider bounds(62, 174, 70, 70), channel("xlow"), range(0, 48, 0, 1, 0.1), text("Low")
rslider bounds(132, 174, 70, 70), channel("fcent"), range(10, 20000, 1000, 0.5, 1), text("Centre")
rslider bounds(202, 174, 70, 70), channel("xhigh"), range(0, 48, 0, 1, 0.1), text("High")
rslider bounds(292, 174, 70, 70), channel("room"), range(0, 1, 0.9, 1, .01), text("Room Size")
rslider bounds(362, 174, 70, 70), channel("damp"), range(0, 1, 0.35, 1, .01), text("Damping")
rslider bounds(432, 174, 70, 70), channel("wet"), range(0, 1, 0.5, 1, .01), text("Dry/Wet")
rslider bounds(522, 174, 70, 70), channel("volume"), range(0, 2, 0.2, 0.5, 0.001), text("Volume")
vslider bounds(590, 169, 30, 75), channel("vumeter"), identChannel("vumeter_id"), range(0, 2, 1, 0.5), text("VU"), colour(0,0,0,0)


; soft keyboard:

keyboard bounds(14, 280, 648, 95), value(36)

</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
; Initialize the global variables. 
ksmps = 4 ; if possible stay with <=4 to avoid tickle noise
nchnls = 2
0dbfs = 1

massign 0, 1

gaDry init 0

instr 1

ifrqs ftgentmp 0, 0, 32, 7, 0, 32, 0
iamps ftgentmp 0, 0, 32, 7, 0, 32, 0
idamp ftgentmp 0, 0, 32, 7, 0, 32, 0

iBase = p4
iAmp = p5
iVexp chnget "vexp"
iPbrange chnget "pbend"
kVib chnget "vib"
kFvib chnget "fvib"
kTrem chnget "trem"
kFtrem chnget "ftrem"
iNpart chnget "npart"
iFexp chnget "fexp"
ifdamp chnget "pdecay"
iA chnget "A"
iB chnget "B"
iC chnget "C"
iAtt chnget "att"
iXatt chnget "xatt"
iDec chnget "dec"
iXdec chnget "xdec"
iSus chnget "sus"
iRel chnget "rel"
iRelv chnget "relv"
iXlow chnget "xlow"
iXhigh chnget "xhigh"
iFcent chnget "fcent"
kVolume chnget "volume"

; During the release phase get release velocity:
kdata2 init 0
krel release
if (krel==1)&&(kdata2==0) then
  kstatus, kchan, kdata1, kdata2 midiin
  kEnvexp =1/(1-(1-iRelv)*(kdata2-1)/127)
endif

; Modify velocity curve (gamma correction):
iAmp = iAmp^iVexp

; Appply pitch bend:
kpb pchbend 0,iPbrange
kBase = iBase * 1.059463^kpb

; Calculate the partials and damping factors:
index = 0
ifreq = 1
ifreql = 1
isumamp = 0
loop:
  prints "%i ", ifreq
  ipfreq = ifreq*iBase
  ipamp = ( ipfreq<=(sr/2) ? (ifreq^(-iFexp)) * ((1+(iFcent/ipfreq))^(-iXlow/12)) * ((1+(ipfreq/iFcent))^(-iXhigh/12)) : 0 )
  isumamp = isumamp + ipamp^2
  tablew ifreq, index, ifrqs
  tablew ipamp, index, iamps
  idmp = exp ( - (ksmps/sr) * ifdamp * (ipfreq-iBase) )
  tablew idmp, index, idamp
  ifreqx = ifreq
  ifreq = iA*ifreq + iB*ifreql + iC
  ifreql = ifreqx
loop_lt index,1,iNpart,loop
prints "\n"

; Dampen higher partials over time:
kindex = 1
loopk:
  kamp1 table kindex, iamps
  kdmp1 table kindex, idamp
  tablew kamp1*kdmp1, kindex, iamps
loop_lt kindex,1,iNpart,loopk

; Calculate vibrato and tremolo:
kVibo lfo kVib, kFvib
kTremo lfo kTrem, kFtrem

; Generate sound:
asig adsynt2 kVolume*iAmp/sqrt(isumamp)*(1.122018^kTremo), kBase*(1.059463^kVibo), -1, ifrqs, iamps, iNpart, 2

; Calculate envelope:
iAtt=iAtt/((iBase/27.5)^iXatt)
iDec=iDec/((iBase/440)^iXdec)
kEnvraw transegr 0, iAtt, 0, 1, 10-iAtt, (iAtt-10)/iDec, iSus, iRel, -10,0

; Modify envelope during release phase:
kStartRel init 0
if krel==0 then
  kEnv = kEnvraw
else
  if kEnvraw==0 then
    kEnv = 0
  else
    if kStartRel==0 then
      kStartRel = kEnvraw
    endif
    kEnv = (kEnvraw/kStartRel)^kEnvexp*kStartRel
  endif
endif

; If used with reverb:
gaDry = gaDry + asig*kEnv

; If used without reverb:
; outs gaDry, gaDry

endin


; This instrument sets the sliders from MIDI controls and controls the coupling of sliders:

instr 2

kMvib chnget "mvib"
kMtrem chnget "mtrem"
kFcouple chnget "mfcouple"
kFvib chnget "fvib"
kFtrem chnget "ftrem"

if (kMvib!=0) then
  kVibm ctrl7 1, 1, 0, 1
  chnset kVibm, "vib"
endif

if (kMtrem!=0) then
  kTremm ctrl7 1, 1, 0, 5
  chnset kTremm, "trem"
endif

if (kFcouple!=0) then
  kCfvib changed kFvib
  if (kCfvib==1) then
    chnset kFvib, "ftrem"
  endif
  kCftrem changed kFtrem
  if (kCftrem==1) then
    chnset kFtrem, "fvib"
  endif
endif

endin


; This instrument adds the reverb:

instr 10

kRoom chnget "room"
kDamp chnget "damp"
kWet chnget "wet"

aLrev, aRrev freeverb gaDry, gaDry, kRoom, kDamp, sr, 0
kDry = 1 - kWet
aLtot = kDry*gaDry + kWet*aLrev
aRtot = kDry*gaDry + kWet*aRrev

outs aLtot, aRtot

ktrig metro 25
kVU max_k gaDry, ktrig, 1
if (ktrig==1) then
  chnset kVU, "vumeter"
  if (kVU>1) then
    chnset "trackerColour(255,0,0)", "vumeter_id"
  else
    chnset "trackerColour(0,255,0)", "vumeter_id"
  endif
endif

clear gaDry

endin


</CsInstruments>
<CsScore>
i2 0 z
i10 0 z
</CsScore>
</CsoundSynthesizer>
